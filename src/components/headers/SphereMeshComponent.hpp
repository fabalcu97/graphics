# pragma once

#include "TransformComponent.hpp"
#include "../../structures/headers/Triangle.hpp"
#include <vector>

class SphereMeshComponent: public TransformComponent {
  private:
    std::vector<Triangle> TrianglesSphereTemplate;
    std::vector<Vector> VerticesSphereTemplate;
    std::vector<Vector> VerticesPSphereTemplate;// phi, theta, radio
    std::vector<Vector> NormalsSphereTemplate;// phi, theta, radio
    //faltan los UV0
    Vector PuntoTFromAToBEsferico(Vector a, Vector b, float t);
    void DividirTriangle(Triangle t);
    Vector PuntoTFromAToB(Vector a, Vector b, float t);
    void DividirTriangles();
    void CreateSphereTemplate(int Precision);


  public:
    SphereMeshComponent();
    ~SphereMeshComponent();

    void virtual draw() override;
    //void virtual render() override;
    void virtual update(float DeltaTime) override;
    void virtual beginPlay() override;
    void virtual endPlay() override;
};
