#pragma once

#ifdef __APPLE__
    #include <OpenGL/glu.h>
#elif __linux__
    #include <GLU/glu.h>
#endif
#include <GLFW/glfw3.h>
#include <vector>
#include <algorithm>
#include "Component.hpp"
#include "../../structures/headers/Vector.hpp"
#include "../../structures/headers/Rotator.hpp"

class TransformComponent: public Component {
  private:

  public:
    Vector Position;
    Vector Scale;
    Rotator Rotation;
    Vector WorldPosition;
    Vector WorldScale;
    Rotator WorldRotation;
    TransformComponent* Parent;
    std::vector<TransformComponent *> Sons;
    void AttachToComponent(TransformComponent * NewParent, bool KeepWorldPosition = false);
    TransformComponent();
    ~TransformComponent();

    virtual void draw();
    virtual void ObtainWorldValues();
    virtual void render() override;
    virtual void update(float DeltaTime) override;
    virtual void beginPlay() override;
    virtual void endPlay() override;

    Vector GetRelativePosition ();
    void SetRelativePosition (Vector NewPosition);
    Vector GetGlobalPosition ();
    void SetGlobalPosition (Vector NewPosition);

    Vector GetRelativeScale();
    void SetRelativeScale(Vector NewScale);
    Vector GetGlobalScale();
    void SetGlobalScale(Vector NewScale);

    Rotator GetRelativeRotation();
    void SetRelativeRotation(Rotator NewRotation);
    Rotator GetGlobalRotation();
    void SetGlobalRotation(Rotator NewRotation);
};