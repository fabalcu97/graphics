#pragma once

#include "CollisionComponent.hpp"

class BoxCollisionComponent: public CollisionComponent {
    private:
        Vector Extent;
    public:
        void SetExtent(Vector NewExtent);
        void SetWorldExtent(Vector NewExtent);
        Vector GetNegativePoint();
        Vector GetPositvePoint();
        virtual void draw() override;
        virtual void update(float DeltaTime) override;
        virtual void beginPlay() override;
        virtual void endPlay() override;
        virtual void Destroy() override;
        void AreaOverlapBoxCollision(BoxCollisionComponent & OtherCollision);
        BoxCollisionComponent();
        ~BoxCollisionComponent();
};
