#pragma once

#include <string>
#include "../../engine/Object.hpp"


class Component: public Object {
    private:
        std::string Name;
    public:
        //Object * Owner;
        class GameWorld * World;
        Component();
        ~Component();

        void SetName(std::string NewName);
        std::string GetName();
        virtual void render() override;
        virtual void update(float DeltaTime) override;
        virtual void beginPlay() override;
        virtual void endPlay() override;
        virtual void Destroy();
};