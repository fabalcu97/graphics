# pragma once

#include "TransformComponent.hpp"
#include "../../structures/headers/Vector.hpp"

class CameraComponent: public TransformComponent {
private:
    bool TypeProjection;//0 perspective, 1 ortho
    float FieldOfView;
    Vector LookAt;
    //la relacion de aspecto se calcula tomando como base el viewport o tamaño de la ventaba, cundo renderice debo estar recibiendo este valor de aguna forma
    Vector GlobalCamPosition;//aun que esto podria sacarla del transform, por ahora dejarlo asi
    //a donde enfoco es deberia ser un componente TransforComponent hijo, para que se calcule solo.
    //o tambien podria buscar la manera de obtener su vector hacia adeltante, y en funcion de ello poduo saber hacia donde apunta, igual su vector up

public:
    void CamRender();
    CameraComponent();
    ~CameraComponent();
};
