#pragma once

#include "TransformComponent.hpp"
//#include "../../actors/headers/Actor.hpp"
#include <vector>
#include <functional>

class Actor;

class CollisionComponent: public TransformComponent {
private:
    std::vector<Actor *> OverlappedActors;
    std::vector<CollisionComponent *> OverlappedComponents;
public:
    virtual void CheckCollision(CollisionComponent * OtherCollision);
    bool IsOverlappedWith(CollisionComponent * OtherCollision);
    std::vector<CollisionComponent *> GetOverlappedComponents();
    std::vector<Actor *> GetOverlappedActors();
    std::vector<std::function<void(CollisionComponent *, Actor *)>> BindsBeginOverlap;//muchos binds
    template<class T>
    void BindBeginOverlap(T* const object, void(T::* const functioncallback)(CollisionComponent *, Actor *)) {
        using namespace std::placeholders; 
        BindsBeginOverlap.emplace_back(std::bind(functioncallback, object, _1, _2));
    }

    std::vector<std::function<void(CollisionComponent *, Actor *)>> BindsEndOverlap;//muchos binds
    template<class T>
    void BindEndOverlap(T* const object, void(T::* const functioncallback)(CollisionComponent *, Actor *)) {
        using namespace std::placeholders; 
        BindsEndOverlap.emplace_back(std::bind(functioncallback, object, _1, _2));
    }

    /*std::vector<std::function<void()>> Callbacks; ejemplo sin parametros
    template<class T>
    void AddCallback(T* const object, void(T::* const functioncallback)()) {
        using namespace std::placeholders; 
        Callbacks.emplace_back(std::bind(functioncallback, object));
    }*/
    void BeginOverlapComponent(CollisionComponent * OtherComp, Actor * OtherActor);
    void EndOverlapComponent(CollisionComponent * OtherComp, Actor * OtherActor);
    virtual void draw() override;
    virtual void update(float DeltaTime) override;
    virtual void beginPlay() override;
    virtual void endPlay() override;
    CollisionComponent();
    ~CollisionComponent();
};

