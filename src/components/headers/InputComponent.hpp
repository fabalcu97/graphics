#pragma once

#include "Component.hpp"
#include "../../systems/headers/InputSystem.hpp"


class InputComponent: public Component {
    private:
        InputSystem * inputSystem;

        std::vector<std::pair<std::string, int> > bindAxisFunctions;
        std::vector<std::pair<std::string, int> > bindPressFunctions;
        std::vector<std::pair<std::string, int> > bindReleaseFunctions;

    public:
        InputComponent();
        ~InputComponent();
        void SetInputSystem(InputSystem * input);
        void virtual Destroy() override;

        template<class T>
        void BindAxis(std::string axisName, T* object, void(T::* const callback)(float)) {
            if (inputSystem) {
                int index = inputSystem->BindAxis(axisName, object, callback);
                bindAxisFunctions.push_back(std::make_pair(axisName, index));
            }
        }

        template<class T>
        void BindAction(std::string actionName, int action, T* object, void(T::* const callback)()) {
            if (inputSystem) {
                int index = inputSystem->BindAction(actionName, action, object, callback);
                if(action == PRESS) {
                    bindPressFunctions.push_back(std::make_pair(actionName, index));
                }
                else if(action == RELEASE) {
                    bindReleaseFunctions.push_back(std::make_pair(actionName, index));
                }
            }
        }

};

