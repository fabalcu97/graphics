#pragma once

#include "CollisionComponent.hpp"

class SphereCollisionComponent: public CollisionComponent {
private:
    float Radio;
public:

    void SetRadio(float NewRadio);
    void virtual draw() override;
    void virtual update(float DeltaTime) override;
    void virtual beginPlay() override;
    void virtual endPlay() override;
    SphereCollisionComponent();
    ~SphereCollisionComponent();
};

