#include "../headers/SphereMeshComponent.hpp"
#ifdef __APPLE__
    #include <cmath>
#elif __linux__
    #include <math.h>
#endif

#define PI 3.1415926535897

SphereMeshComponent::SphereMeshComponent(){
    CreateSphereTemplate(3);
}

SphereMeshComponent::~SphereMeshComponent(){

}

void SphereMeshComponent::beginPlay(){

}

void SphereMeshComponent::draw(){
    glBegin(GL_TRIANGLES);
    glColor3d(0, 0, 200);
    float sphereAmbient[] = {0.0f, 0.1f, 0.9f, 1.0f};
    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, sphereAmbient);

    float sphereDiffuse[] = {0.0f, 0.1f, 0.9f, 1.0f};
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, sphereDiffuse);

    float sphereSpecular[] = {0.0f, 0.1f, 0.7f, 1.0f};
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, sphereSpecular);

    float sphereShininess = 0.5;
    glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, sphereShininess);
    
    for(int i = 0; i < TrianglesSphereTemplate.size(); i++){
        glNormal3f(NormalsSphereTemplate[TrianglesSphereTemplate[i].IdC].X, NormalsSphereTemplate[TrianglesSphereTemplate[i].IdC].Y, NormalsSphereTemplate[TrianglesSphereTemplate[i].IdC].Z);
        glVertex3f(VerticesSphereTemplate[TrianglesSphereTemplate[i].IdC].X, VerticesSphereTemplate[TrianglesSphereTemplate[i].IdC].Y, VerticesSphereTemplate[TrianglesSphereTemplate[i].IdC].Z);

        glNormal3f(NormalsSphereTemplate[TrianglesSphereTemplate[i].IdB].X, NormalsSphereTemplate[TrianglesSphereTemplate[i].IdB].Y, NormalsSphereTemplate[TrianglesSphereTemplate[i].IdB].Z);
        glVertex3f(VerticesSphereTemplate[TrianglesSphereTemplate[i].IdB].X, VerticesSphereTemplate[TrianglesSphereTemplate[i].IdB].Y, VerticesSphereTemplate[TrianglesSphereTemplate[i].IdB].Z);

        glNormal3f(NormalsSphereTemplate[TrianglesSphereTemplate[i].IdA].X, NormalsSphereTemplate[TrianglesSphereTemplate[i].IdA].Y, NormalsSphereTemplate[TrianglesSphereTemplate[i].IdA].Z);
        glVertex3f(VerticesSphereTemplate[TrianglesSphereTemplate[i].IdA].X, VerticesSphereTemplate[TrianglesSphereTemplate[i].IdA].Y, VerticesSphereTemplate[TrianglesSphereTemplate[i].IdA].Z);
    }
    glEnd();
}

//funciona sin necesidad de sobreescribir
/*void SphereMeshComponent::render(){
    glPushMatrix();
    glTranslatef(Position.X, Position.Y, Position.Z);
    glScalef(Scale.X, Scale.Y, Scale.Z);
    draw();
    //llamar a render de los hijos
    for(int i = 0; i < Sons.size(); i++){
        Sons[i]->render();
    }
    glPopMatrix();
}*/

void SphereMeshComponent::update(float DeltaTime){

}

void SphereMeshComponent::endPlay(){

}

Vector SphereMeshComponent::PuntoTFromAToB(Vector a, Vector b, float t) {
    Vector direccion = b - a;
    Vector punto = a + direccion * t;
    //normalizar a esfera de radio 1
    punto = punto.Normalize();
    return punto;
}

Vector SphereMeshComponent::PuntoTFromAToBEsferico(Vector a, Vector b, float t) {
    Vector direccion = b - a;
    Vector punto = a + direccion * t;
    //normalizar a esfera de radio 1
    punto = punto.Normalize();
    float phi = std::acos(punto.Z);
    float theta = std::asin(punto.Y / std::sin(phi));
    Vector puntoesferico (phi, theta, 1.0f);
    return puntoesferico;
}

void SphereMeshComponent::DividirTriangle(Triangle t) {
    Vector D = PuntoTFromAToB(t.C, t.A, 0.5);
    VerticesSphereTemplate.push_back(D);//0.5 es la mitad
    int IdD = VerticesSphereTemplate.size() - 1;
    Vector DP = PuntoTFromAToBEsferico(t.C, t.A, 0.5);
    VerticesPSphereTemplate.push_back(DP);//0.5 es la mitad
    NormalsSphereTemplate.push_back(D);
    //TangentsSphereTemplate.push_back(FProcMeshTangent(1.0f * FMath::Sin(PI/2)*FMath::Cos(DP.Y + PI/2), 1.0f * FMath::Sin(PI/2)*FMath::Sin(DP.Y + PI/2), 1.0f * FMath::Cos(PI/2)));
    //UV0SphereTemplate.push_back(FVector2D(DP.Y/(2*PI), 1 - DP.X/PI));
    //VertexColorsSphereTemplate.push_back(FLinearColor(0.75, 0.75, 0.75, 1.0));

    Vector E = PuntoTFromAToB(t.B, t.A, 0.5);
    VerticesSphereTemplate.push_back(E);//0.5 es la mitad
    int IdE = VerticesSphereTemplate.size() - 1;
    Vector EP = PuntoTFromAToBEsferico(t.B, t.A, 0.5);
    VerticesPSphereTemplate.push_back(EP);//0.5 es la mitad
    NormalsSphereTemplate.push_back(E);
    //TangentsSphereTemplate.push_back(FProcMeshTangent(1.0f * FMath::Sin(PI/2)*FMath::Cos(EP.Y + PI/2), 1.0f * FMath::Sin(PI/2)*FMath::Sin(EP.Y + PI/2), 1.0f * FMath::Cos(PI/2)));
    //UV0SphereTemplate.push_back(FVector2D(EP.Y/(2*PI), 1 - EP.X/PI));
    //VertexColorsSphereTemplate.push_back(FLinearColor(0.75, 0.75, 0.75, 1.0));

    Vector F = PuntoTFromAToB(t.C, t.B, 0.5);
    VerticesSphereTemplate.push_back(F);//0.5 es la mitad
    int IdF = VerticesSphereTemplate.size() - 1;
    Vector FP = PuntoTFromAToBEsferico(t.C, t.B, 0.5);
    VerticesPSphereTemplate.push_back(FP);//0.5 es la mitad
    NormalsSphereTemplate.push_back(F);
    //TangentsSphereTemplate.push_back(FProcMeshTangent(1.0f * FMath::Sin(PI/2)*FMath::Cos(FP.Y + PI/2), 1.0f * FMath::Sin(PI/2)*FMath::Sin(FP.Y + PI/2), 1.0f * FMath::Cos(PI/2)));
    //UV0SphereTemplate.push_back(FVector2D(FP.Y/(2*PI), 1 - FP.X/PI));
    //VertexColorsSphereTemplate.push_back(FLinearColor(0.75, 0.75, 0.75, 1.0));

    //if (VerticesP[t.IdA].X == VerticesP[t.IdC].X) {//tringulo hacia abajo
    if (t.Orientacion) {//tringulo hacia abajo
        TrianglesSphereTemplate.push_back(Triangle(t.A, E, D, t.IdA, IdE, IdD, t.Nivel*2, 0));
        TrianglesSphereTemplate.push_back(Triangle(D, E, F, IdD, IdE, IdF, t.Nivel*2, 0));
        TrianglesSphereTemplate.push_back(Triangle(D, F, t.C, IdD, IdF, t.IdC, t.Nivel*2, 0));
        TrianglesSphereTemplate.push_back(Triangle(E, t.B, F, IdE, t.IdB, IdF, t.Nivel*2 + 1, 1));
    }
    else {//triangulo hacia arriba, por lo tanto c y b esta a la misma algutar, a arriba
        TrianglesSphereTemplate.push_back(Triangle(t.A, E, D, t.IdA, IdE, IdD, t.Nivel*2, 0));
        TrianglesSphereTemplate.push_back(Triangle(E, t.B, F, IdE, t.IdB, IdF, t.Nivel*2 + 1, 1));
        TrianglesSphereTemplate.push_back(Triangle(E, F, D, IdE, IdF, IdD, t.Nivel*2+1, 1));
        TrianglesSphereTemplate.push_back(Triangle(D, F, t.C, IdD, IdF, t.IdC, t.Nivel*2 + 1, 1));
    }
}

void SphereMeshComponent::DividirTriangles() {
    int n = TrianglesSphereTemplate.size();
    for (int i = 0; i < n; i++) {
        DividirTriangle(TrianglesSphereTemplate[0]);
        TrianglesSphereTemplate.erase(TrianglesSphereTemplate.begin());
    }
}


void SphereMeshComponent::CreateSphereTemplate(int Precision) {
    float DeltaPhi = 1.10714872;
    float DeltaTheta = PI * 72 / 180;
    float DiffTheta = PI * 36 / 180;
    VerticesSphereTemplate.push_back(Vector(0.0f, 0.0f, 1.0f));//puntos para radio uno
    NormalsSphereTemplate.push_back(Vector(0.0f, 0.0f, 1.0f));//puntos para radio uno
    //Tangents.Add(FProcMeshTangent(1.0f, 0.0f, 0.0f));
    //TangentsSphereTemplate.push_back(FProcMeshTangent(1.0f * FMath::Sin(PI/2)*FMath::Cos(PI/2), 1.0f * FMath::Sin(PI/2)*FMath::Sin(PI/2), 1.0f * FMath::Cos(PI/2)));
    VerticesPSphereTemplate.push_back(Vector(0.0f, 0.0f, 1.0f));
    //UV0SphereTemplate.push_back(FVector2D(0.0f,1.0f));
	//VertexColorsSphereTemplate.push_back(FLinearColor(0.75, 0.75, 0.75, 1.0));
    for (int i = 0; i < 5; i++) {
        VerticesPSphereTemplate.push_back(Vector(DeltaPhi, i*DeltaTheta, 1.0f));//esto es en esferico, no se para que lo uso desues, creo que para las divisiones para aumentr la suavidad
        Vector posicion;
        posicion.X = 1.0f * std::sin(DeltaPhi) * std::cos(i*DeltaTheta);
        posicion.Y = 1.0f * std::sin(DeltaPhi) * std::sin(i*DeltaTheta);
        posicion.Z = 1.0f * std::cos(DeltaPhi);
        //hasta aqui posicion esta como unitario, puede servir para el verctor de normales
        VerticesSphereTemplate.push_back(posicion);
        NormalsSphereTemplate.push_back(posicion);
        //TangentsSphereTemplate.push_back(FProcMeshTangent(1.0f * FMath::Sin(PI/2)*FMath::Cos(i*DeltaTheta + PI/2), 1.0f * FMath::Sin(PI/2)*FMath::Sin(i*DeltaTheta + PI/2), 1.0f * FMath::Cos(PI/2)));
        //UV0SphereTemplate.push_back(FVector2D(i*DeltaTheta/(2*PI), 1 - DeltaPhi/PI));
        //VertexColorsSphereTemplate.push_back(FLinearColor(0.75, 0.75, 0.75, 1.0));
    }
    for (int i = 0; i < 5; i++) {
        VerticesPSphereTemplate.push_back(Vector(PI - DeltaPhi, i*DeltaTheta + DiffTheta, 1.0f));
        Vector posicion;
        posicion.X = 1.0f * std::sin(PI - DeltaPhi) * std::cos(i*DeltaTheta + DiffTheta);
        posicion.Y = 1.0f * std::sin(PI - DeltaPhi) * std::sin(i*DeltaTheta + DiffTheta);
        posicion.Z = 1.0f * std::cos(PI - DeltaPhi);
        //hasta aqui posicion esta como unitario, puede servir para el verctor de normales
        NormalsSphereTemplate.push_back(posicion);
        //TangentsSphereTemplate.push_back(FProcMeshTangent(1.0f * FMath::Sin(PI/2)*FMath::Cos(i*DeltaTheta + PI/2), 1.0f * FMath::Sin(PI/2)*FMath::Sin(i*DeltaTheta + PI/2), 1.0f * FMath::Cos(PI/2)));
        VerticesSphereTemplate.push_back(posicion);
        //UV0SphereTemplate.push_back(FVector2D((i*DeltaTheta + DiffTheta)/(2*PI), 1 - (PI - DeltaPhi)/PI));
        //VertexColorsSphereTemplate.push_back(FLinearColor(0.75, 0.75, 0.75, 1.0));
    }
    VerticesSphereTemplate.push_back(Vector(0.0, 0.0f, -1.0f));//puntos para radio uno
    NormalsSphereTemplate.push_back(Vector(0.0, 0.0f, -1.0f));//puntos para radio uno
    //Tangents.Add(FProcMeshTangent(-1.0f, 0.0f, 0.0f));
    //TangentsSphereTemplate.push_back(FProcMeshTangent(1.0f * FMath::Sin(PI/2)*FMath::Cos(PI/2), 1.0f * FMath::Sin(PI/2)*FMath::Sin(PI/2), 1.0f * FMath::Cos(PI/2)));
    VerticesPSphereTemplate.push_back(Vector(PI, 0.0f, 1.0f));
    //UV0SphereTemplate.push_back(FVector2D(0.0f,0.0f));
    //VertexColorsSphereTemplate.push_back(FLinearColor(0.75, 0.75, 0.75, 1.0));

    //Agregados vertices y normales, faltarian agregar las tangentes

    //Tirangulos superiores
    TrianglesSphereTemplate.push_back(Triangle(VerticesSphereTemplate[0], VerticesSphereTemplate[1], VerticesSphereTemplate[2], 0, 1, 2, 0, 0));
    TrianglesSphereTemplate.push_back(Triangle(VerticesSphereTemplate[0], VerticesSphereTemplate[2], VerticesSphereTemplate[3], 0, 2, 3, 0, 0));
    TrianglesSphereTemplate.push_back(Triangle(VerticesSphereTemplate[0], VerticesSphereTemplate[3], VerticesSphereTemplate[4], 0, 3, 4, 0, 0));
    TrianglesSphereTemplate.push_back(Triangle(VerticesSphereTemplate[0], VerticesSphereTemplate[4], VerticesSphereTemplate[5], 0, 4, 5, 0, 0));
    TrianglesSphereTemplate.push_back(Triangle(VerticesSphereTemplate[0], VerticesSphereTemplate[5], VerticesSphereTemplate[1], 0, 5, 1, 0, 0));

    //triangulos medios
    TrianglesSphereTemplate.push_back(Triangle(VerticesSphereTemplate[1], VerticesSphereTemplate[6], VerticesSphereTemplate[2], 1, 6, 2, 1, 1));
    TrianglesSphereTemplate.push_back(Triangle(VerticesSphereTemplate[2], VerticesSphereTemplate[6], VerticesSphereTemplate[7], 2, 6, 7, 1, 0));
    TrianglesSphereTemplate.push_back(Triangle(VerticesSphereTemplate[2], VerticesSphereTemplate[7], VerticesSphereTemplate[3], 2, 7, 3, 1, 1));
    TrianglesSphereTemplate.push_back(Triangle(VerticesSphereTemplate[3], VerticesSphereTemplate[7], VerticesSphereTemplate[8], 3, 7, 8, 1, 0));
    TrianglesSphereTemplate.push_back(Triangle(VerticesSphereTemplate[3], VerticesSphereTemplate[8], VerticesSphereTemplate[4], 3, 8, 4, 1, 1));
    TrianglesSphereTemplate.push_back(Triangle(VerticesSphereTemplate[4], VerticesSphereTemplate[8], VerticesSphereTemplate[9], 4, 8, 9, 1, 0));
    TrianglesSphereTemplate.push_back(Triangle(VerticesSphereTemplate[4], VerticesSphereTemplate[9], VerticesSphereTemplate[5], 4, 9, 5, 1, 1));
    TrianglesSphereTemplate.push_back(Triangle(VerticesSphereTemplate[5], VerticesSphereTemplate[9], VerticesSphereTemplate[10], 5, 9, 10, 1, 0));
    TrianglesSphereTemplate.push_back(Triangle(VerticesSphereTemplate[5], VerticesSphereTemplate[10], VerticesSphereTemplate[1], 5, 10, 1, 1, 1));
    TrianglesSphereTemplate.push_back(Triangle(VerticesSphereTemplate[1], VerticesSphereTemplate[10], VerticesSphereTemplate[6], 1, 10, 6, 1, 0));

    //triangulos inferiores
    TrianglesSphereTemplate.push_back(Triangle(VerticesSphereTemplate[6], VerticesSphereTemplate[11], VerticesSphereTemplate[7], 6, 11, 7, 2, 1));
    TrianglesSphereTemplate.push_back(Triangle(VerticesSphereTemplate[7], VerticesSphereTemplate[11], VerticesSphereTemplate[8], 7, 11, 8, 2, 1));
    TrianglesSphereTemplate.push_back(Triangle(VerticesSphereTemplate[8], VerticesSphereTemplate[11], VerticesSphereTemplate[9], 8, 11, 9, 2, 1));
    TrianglesSphereTemplate.push_back(Triangle(VerticesSphereTemplate[9], VerticesSphereTemplate[11], VerticesSphereTemplate[10], 9, 11, 10, 2, 1));
    TrianglesSphereTemplate.push_back(Triangle(VerticesSphereTemplate[10], VerticesSphereTemplate[11], VerticesSphereTemplate[6], 10, 11, 6, 2, 1));

    while (Precision) {
        DividirTriangles();
        Precision--;
    }
}
