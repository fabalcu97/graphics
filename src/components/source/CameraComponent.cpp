#include "../headers/CameraComponent.hpp"

CameraComponent::CameraComponent(){
    FieldOfView = 90.0f;
    TypeProjection = 0;
    Position = Vector (-20.0f, 40.0f, 0.0f);
}

CameraComponent::~CameraComponent(){

}

void CameraComponent::CamRender(){//debe rebiri cositas
    if(TypeProjection){
        glTranslatef(Position.X, Position.Y, Position.Z);
        glOrtho(-50.0f,  50.0f, -50.0f, 50.0f, -50.0f, 50.0f); //usaremos una proyeccion oroto grafica, que va de -50 a 50 en ambos ejes
    }
    else{
        gluPerspective(FieldOfView, 1, 1, 200.0f);
        gluLookAt(Position.X, Position.Y, Position.Z, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);//esto va al inicio de todos los calculos o llamadas paar dibujar algo
        //no se si estao es ya no necesita que haga el translate
    }
}