#ifdef __APPLE__
    #include <OpenGL/glu.h>
#elif __linux__
    #include <GLU/glu.h>
#endif
#include <GLFW/glfw3.h>
#include <iostream>

#include "../headers/BoxMeshComponent.hpp"

BoxMeshComponent::BoxMeshComponent () {
    int index = 0;
    // for (int i = 0; i <= 99; i += 3) {
    //     std::cout << index << std::endl;
    //     Vector a = Vector(vertex[i], vertex[i+1], vertex[i+2]);
    //     i += 3;
    //     Vector b = Vector(vertex[i], vertex[i+1], vertex[i+2]);
    //     i += 3;
    //     Vector c = Vector(vertex[i], vertex[i+1], vertex[i+2]);
    //     index = (int) (i / 9) - 1;
    //     Vector diff = Vector(c - a);
    //     normalBoxVector[index] = (b - a).CrossProduct(diff).Normalize();
    // }
}

BoxMeshComponent::~BoxMeshComponent () {

}

void BoxMeshComponent::draw () {
    glPushMatrix();
        glBegin(GL_TRIANGLES);
            glColor3d(0, 0, 200);
            float ambient[] = {0.9f, 0.1f, 0.0f, 1.0f};
            glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, ambient);

            float diffuse[] = {0.9f, 0.1f, 0.0f, 1.0f};
            glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, diffuse);

            float specular[] = {0.7f, 0.1f, 0.0f, 1.0f};
            glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, specular);

            float shininess = 0.5;
            glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, shininess);
            int index = 0;
            // bottom
            glNormal3f(0.0f, -1.0f, 0.0f);
            glVertex3f(1.0f, -1.0f, -1.0f);
            glVertex3f(1.0f, -1.0f, 1.0f);
            glVertex3f(-1.0f, -1.0f, 1.0f);
            glVertex3f(1.0f, -1.0f, -1.0f);
            glVertex3f(-1.0f, -1.0f, 1.0f);
            glVertex3f(-1.0f, -1.0f, -1.0f);
            //top
            glNormal3f(0.0f, 1.0f, 0.0f);
            glVertex3f(1.0f, 1.0f, -1.0f);
            glVertex3f(-1.0f, 1.0f, -1.0f);
            glVertex3f(-1.0f, 1.0f, 1.0f);
            glVertex3f(1.0f, 1.0f, -1.0f);
            glVertex3f(-1.0f, 1.0f, 1.0f);
            glVertex3f(1.0f, 1.0f, 1.0f);
            //right
            glNormal3f(1.0f, 0.0f, 0.0f);
            glVertex3f(1.0f, -1.0f, -1.0f);
            glVertex3f(1.0f, 1.0f, -1.0f);
            glVertex3f(1.0f, 1.0f, 1.0f);
            glVertex3f(1.0f, -1.0f, -1.0f);
            glVertex3f(1.0f, 1.0f, 1.0f);
            glVertex3f(1.0f, -1.0f, 1.0f);
            //front
            glNormal3f(0.0f, 0.0f, 1.0f);
            glVertex3f(1.0f, -1.0f, 1.0f);
            glVertex3f(1.0f, 1.0f, 1.0f);
            glVertex3f(-1.0f, 1.0f, 1.0f);
            glVertex3f(1.0f, -1.0f, 1.0f);
            glVertex3f(-1.0f, 1.0f, 1.0f);
            glVertex3f(-1.0f, -1.0f, 1.0f);
            //left
            glNormal3f(-1.0f, 0.0f, 0.0f);
            glVertex3f(-1.0f, -1.0f, 1.0f);
            glVertex3f(-1.0f, 1.0f, 1.0f);
            glVertex3f(-1.0f, 1.0f, -1.0f);
            glVertex3f(-1.0f, -1.0f, 1.0f);
            glVertex3f(-1.0f, 1.0f, -1.0f);
            glVertex3f(-1.0f, -1.0f, -1.0f);
            //back
            glNormal3f(0.0f, 0.0f, -1.0f);
            glVertex3f(1.0f, 1.0f, -1.0f);
            glVertex3f(1.0f, -1.0f, -1.0f);
            glVertex3f(-1.0f, -1.0f, -1.0f);
            glVertex3f(1.0f, 1.0f, -1.0f);
            glVertex3f(-1.0f, -1.0f, -1.0f);
            glVertex3f(-1.0f, 1.0f, -1.0f);
        glEnd();
    glPopMatrix();
}

// void BoxMeshComponent::render () {
    // glPushMatrix();
    //     glBegin(GL_TRIANGLES);
    //     glColor3f(250, 0, 0);
    //     for (int i = 0; i <= 108; i += 3) {
    //         glVertex3f(vertex[i], vertex[i+1], vertex[i+2]);
    //     }
    //     glEnd();
    // glPopMatrix();
// }

void BoxMeshComponent::update (float DeltaTime) {

}

void BoxMeshComponent::beginPlay () {

}

void BoxMeshComponent::endPlay () {

}
