#include "../headers/InputComponent.hpp"

#include <iostream>

InputComponent::InputComponent() {
    inputSystem = nullptr;
}

InputComponent::~InputComponent() {
    if ( inputSystem ) {
        delete inputSystem;
    }
}

void InputComponent::SetInputSystem (InputSystem* input) {
    inputSystem = input;
}

void InputComponent::Destroy () {
    std::vector<std::pair<std::string, int> >::iterator it;
    for (it = bindAxisFunctions.begin(); it != bindAxisFunctions.end(); it++) {
        inputSystem->UnbindAxis(it->first, it->second);
    }
    for (it = bindPressFunctions.begin(); it != bindPressFunctions.end(); it++) {
        inputSystem->UnbindPressAction(it->first, it->second);
    }
    for (it = bindReleaseFunctions.begin(); it != bindReleaseFunctions.end(); it++) {
        inputSystem->UnbindReleaseAction(it->first, it->second);
    }
}