#include "../headers/Component.hpp"
#include "../../engine/headers/GameWorld.hpp"

Component::Component() {

}

Component::~Component() {
  
}

void Component::render () {

}

void Component::beginPlay() {

}

void Component::endPlay() {

}

void Component::update(float DeltaTime) {

}

void Component::SetName(std::string NewName){
    Name = NewName;
}
std::string Component::GetName(){
    return Name;
}

void Component::Destroy() {
    if(World){
        World->UnregisterComponent(this);
    }
}