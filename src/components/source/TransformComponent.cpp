#include "../headers/TransformComponent.hpp"

TransformComponent::TransformComponent () {
    Parent = nullptr;
    Position = Vector(0.0f, 0.0f, 0.0f);
    Rotation = Rotator(0.0f, 0.0f, 0.0f);
    Scale = Vector(1.0f, 1.0f, 1.0f);

}

TransformComponent::~TransformComponent () {
    for(int i = 0; i < Sons.size(); i++){
        delete Sons[i];
    }
}

void TransformComponent::ObtainWorldValues(){
    float MatrixGlobal[16];
    glPushMatrix();
    glTranslatef(Position.X, Position.Y, Position.Z);
    glScalef(Scale.X, Scale.Y, Scale.Z);
    glGetFloatv(GL_MODELVIEW_MATRIX, MatrixGlobal);//esto hace algo mas?
    WorldPosition.X = MatrixGlobal[12];
    WorldPosition.Y = MatrixGlobal[13];
    WorldPosition.Z = MatrixGlobal[14];
    for(int i = 0; i < Sons.size(); i++){
        Sons[i]->ObtainWorldValues();
    }
    glPopMatrix();

}

//antes de renderizar debo matar lo que tenga que matar, quiza necesite una funcion PostUpdate o PreRender
void TransformComponent::render(){
    float MatrixGlobal[16];
    glPushMatrix();
    glTranslatef(Position.X, Position.Y, Position.Z);
    glScalef(Scale.X, Scale.Y, Scale.Z);
    glGetFloatv(GL_MODELVIEW_MATRIX, MatrixGlobal);//esto hace algo mas?
    WorldPosition.X = MatrixGlobal[12];
    WorldPosition.Y = MatrixGlobal[13];
    WorldPosition.Z = MatrixGlobal[14];
    draw();
    //llamar a render de los hijos
    for(int i = 0; i < Sons.size(); i++){
        Sons[i]->render();
    }
    glPopMatrix();

}

void TransformComponent::AttachToComponent(TransformComponent * NewParent, bool KeepWorldPosition){
    //debo eliminar de mi padre anterior, si es que existe, mi padre anterior podrai ser el mundo?, el mundo es null?
    if(Parent){
        std::vector<TransformComponent *>::iterator it;
        it = std::find (Parent->Sons.begin(), Parent->Sons.end(), this);
        if (it != Parent->Sons.end()){
            Parent->Sons.erase(it);
        }
    }
    Parent = NewParent;
    Parent->Sons.push_back(this);
    if(KeepWorldPosition){
        //debo hacer calculos para que me quede en la posicion global
    }
    else{
        //debo hacer calculos para quedarme en la posicion relativa
    }
}

void TransformComponent::draw(){

}

void TransformComponent::beginPlay(){

}

void TransformComponent::endPlay(){

}

void TransformComponent::update(float DeltaTime){

}

Vector TransformComponent::GetRelativePosition (){
    return Position;
}

void TransformComponent::SetRelativePosition (Vector NewPosition){
    Position = NewPosition;
}

Vector TransformComponent::GetGlobalPosition (){
    return WorldPosition;
}

void TransformComponent::SetGlobalPosition (Vector NewPosition){
    WorldPosition = NewPosition;
    //deberia relaizar un calculo para obtener actualizar la posicion relativa, por ahora no usaremos mucho lo de las posiciones globales
}

Vector TransformComponent::GetRelativeScale(){
    return Scale;
}

void TransformComponent::SetRelativeScale(Vector NewScale){
    Scale = NewScale;
}

Vector TransformComponent::GetGlobalScale(){
    return WorldScale;
}

void TransformComponent::SetGlobalScale(Vector NewScale){
    WorldScale = NewScale;
}

Rotator TransformComponent::GetRelativeRotation(){
    return Rotation;
}

void TransformComponent::SetRelativeRotation(Rotator NewRotation){
    Rotation = NewRotation;
}

Rotator TransformComponent::GetGlobalRotation(){
    return WorldRotation;
}

void TransformComponent::SetGlobalRotation(Rotator NewRotation){
    WorldRotation = NewRotation;
}