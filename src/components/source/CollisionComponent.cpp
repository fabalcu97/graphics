#include "../headers/CollisionComponent.hpp"
#include "../../actors/headers/Actor.hpp"

CollisionComponent::CollisionComponent() {
}

CollisionComponent::~CollisionComponent() {
}

void CollisionComponent::draw(){

}

void CollisionComponent::beginPlay(){

}

void CollisionComponent::endPlay(){

}

void CollisionComponent::update(float DeltaTime){

}

void CollisionComponent::CheckCollision(CollisionComponent * OtherCollision){

}

void CollisionComponent::BeginOverlapComponent(CollisionComponent * OtherComp, Actor * OtherActor){
    std::cout << "entrando en collision" << std::endl;
    OverlappedComponents.push_back(OtherComp);
    if(OtherActor){
        //el actor debe ser unico, por lo tanto tener cuidado
        OverlappedActors.push_back(OtherActor);
    }
    //llamar a los callbacks
    /*for (const auto& cb : Callbacks){//ejemplo sin parametros
        cb();
    }*/

    for (const auto& bindfunction : BindsBeginOverlap){
        bindfunction(OtherComp, OtherActor);
    }


}

void CollisionComponent::EndOverlapComponent(CollisionComponent * OtherComp, Actor * OtherActor){
    std::cout << "saliendo en collision" << std::endl;
    std::vector<CollisionComponent *>::iterator it;
    it = std::find(OverlappedComponents.begin(), OverlappedComponents.end(), OtherComp);
    OverlappedComponents.erase(it);
    if(OtherActor){
        //el actor debe ser unico, por lo tanto tener cuidado
        std::vector<Actor *>::iterator it;
        it = std::find(OverlappedActors.begin(), OverlappedActors.end(), OtherActor);
        OverlappedActors.erase(it);
    }

    for (const auto& bindfunction : BindsEndOverlap){
        bindfunction(OtherComp, OtherActor);
    }
    //const auto & bindFunction = BindFunctionEndOverlap;
    //bindFunction(OtherComp, OtherActor);
}

std::vector<CollisionComponent *> CollisionComponent::GetOverlappedComponents(){
    return OverlappedComponents;
}

std::vector<Actor *> CollisionComponent::GetOverlappedActors(){
    return OverlappedActors;
}

bool CollisionComponent::IsOverlappedWith(CollisionComponent * OtherCollision){
    std::vector<CollisionComponent *>::iterator it;
    it = std::find(OverlappedComponents.begin(), OverlappedComponents.end(), OtherCollision);
    return it != OverlappedComponents.end();
}