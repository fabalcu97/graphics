#include "../headers/BoxCollisionComponent.hpp"
#include "../../engine/headers/GameWorld.hpp"
#include <iostream>

BoxCollisionComponent::BoxCollisionComponent() {
    Parent = nullptr;
    Extent = Vector(1.0f, 1.0f, 1.0f);
}

BoxCollisionComponent::~BoxCollisionComponent() {
}

void BoxCollisionComponent::draw(){

}

void BoxCollisionComponent::beginPlay(){

}

void BoxCollisionComponent::endPlay(){

}

void BoxCollisionComponent::Destroy(){
    cout << "BoxCollisionComponent: Inciando Destroy" << endl;
    if(World){
        World->UnregisterComponent(this);
    }
    cout << "BoxCollisionComponent: Finalizando Destroy" << endl;
}

void BoxCollisionComponent::update(float DeltaTime){
    // std::cout << "(" << WorldPosition.X << ", " << WorldPosition.Y << ", " << WorldPosition.Z << ") " << std::endl;

}

void BoxCollisionComponent::SetExtent(Vector NewExtent){
    Extent = NewExtent;
}

void BoxCollisionComponent::SetWorldExtent(Vector NewExtent){
    /*
    -------------
    |           |
    |     @     |   Extent.Y
    |           |
    -------------
        Extent.X
        @ (Extent.X/2, Extent.Y/2)
    */
    Extent = NewExtent;
}

Vector BoxCollisionComponent::GetNegativePoint(){
    return WorldPosition - Extent/2;
}

Vector BoxCollisionComponent::GetPositvePoint(){
    return WorldPosition + Extent/2;
}