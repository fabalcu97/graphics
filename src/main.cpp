#define GL_SILENCE_DEPRECATION
#ifdef __APPLE__
    #include <OpenGL/glu.h>
#elif __linux__
    #include <GLU/glu.h>
#endif
#include <GLFW/glfw3.h>
#include <iostream>
#include <vector>

#include "./engine/headers/GameEngine.hpp"
#include "./actors/headers/BoxActor.hpp"
#include "./actors/headers/Character.hpp"
#include "./actors/headers/Enemy.hpp"
#include "./actors/headers/SphereActor.hpp"
#include "./actors/headers/CameraActor.hpp"
#include "./systems/headers/InputSystem.hpp"

void Calculos(){//calculos para la camara, bvan despues de los calculos de render, que es para la escena
	//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);//esto  va antes de render o claculos

	//modo projeccion 
	glMatrixMode(GL_PROJECTION);//entrando en modo projection
	glLoadIdentity();//cargando la identidad
    //aqui se agregand instrucciones al projecyion
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f); //(R, G, B, transparencia) en este caso un fondo negro
    //glRotatef(-45, 0, 0, 1);//angulos en grados
    //glRotatef(55, 1, 1, 0);
    //glRotatef(45, 1, 1, 0);
    gluPerspective(90, 1, 1, 200.0f);
	//glOrtho(-50.0f, 50.0f, -50.0f, 50.0f, -50.0f, 50.0f); //usaremos una proyeccion oroto grafica, que va de -50 a 50 en ambos ejes

    gluPerspective(90, 1, 1, 200.0f);
    gluLookAt(20.0f, 20.0f, 20.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);//esto va al inicio de todos los calculos o llamadas paar dibujar algo
    //aqui se agregand opetaciones en el model view, que es lo que viene despues de llamar a esta funcion



	//glMatrixMode(GL_PROJECTION);
	//glLoadIdentity();
	//glOrtho(-25.0f, 25.0f, -25.0f, 25.0f, -25.0f, 25.0f);
}

void InitGL(){
    
	glEnable(GL_LIGHTING);
	//light 0 "on": try without it
	glEnable(GL_LIGHT0);

	//shading model : try GL_FLAT
	glShadeModel(GL_SMOOTH);
	//glShadeModel(GL_FLAT);

	glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	//enable material : try without it
	glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
	glEnable(GL_TEXTURE_2D);
	//glEnable(GL_COLOR_MATERIAL);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f); //(R, G, B, transparencia) en este caso un fondo negro

	GLfloat position[] = { 10.0f, 5.0f, 10.0f, 0.0 };

	//enable light : try without it
	glLightfv(GL_LIGHT0, GL_POSITION, position);

    float lightAmbient[] = {0.5f, 0.5f, 0.5f, 1.0f};
    glLightfv(GL_LIGHT0, GL_AMBIENT, lightAmbient);

    float lightDiffuse[] = {0.8f, 0.8f, 0.8f, 1.0f};
    glLightfv(GL_LIGHT0, GL_DIFFUSE, lightDiffuse);

    float lightSpecular[] = {0.8f, 0.8f, 0.8f, 1.0f};
    glLightfv(GL_LIGHT0, GL_SPECULAR, lightSpecular);

    //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    //glPolygonMode(GL_FRONT, GL_LINE);

}

InputSystem inputSystem;

void KeyCallback (GLFWwindow* window, int key, int scancode, int action, int mods) {
    inputSystem.KeyCallback(key, action);
}

void CursorCallback (GLFWwindow* window, double xpos, double ypos) {
    inputSystem.CursorCallback(xpos, ypos);
}

void MouseButtonCallback (GLFWwindow* window, int button, int action, int mods) {
    inputSystem.MouseButton(button, action);
}

int main(void) {
    GLFWwindow *window;

    //por alguna razon instanciar objetos fuera del main, da error de multiple definicion al compilar

    GameEngine gameEngine;
    gameEngine.GetWorld()->InputSys = &inputSystem;


    //Creancion de actores
    // BoxActor box (gameEngine.GetWorld(), &inputSystem);
    // box.SetActorRelativeScale(Vector(0.5, 0.5, 0.5));

    Enemy * enemy = gameEngine.GetWorld()->CreateActor<Enemy>();
    enemy->SetActorRelativePosition(Vector(10.0, 10.0, 10.0));
    enemy->SetActorRelativeScale(Vector(0.3, 0.3, 0.3));

    SphereActor * esfera = gameEngine.GetWorld()->CreateActor<SphereActor>();
    esfera->SetActorRelativePosition(Vector(15.0f, 0.0f, 15.0f));
    
    SphereActor * esfera2 = gameEngine.GetWorld()->CreateActor<SphereActor>();
    esfera2->SetActorRelativePosition(Vector(-15.0f, 0.0f, 15.0f));
    
    CameraActor camara;
    Character * Personaje = gameEngine.GetWorld()->CreateActor<Character>();
    //Personaje->Input->SetInputSystem(&inputSystem);
    Personaje->SetActorRelativePosition(Vector(0.0f, 0.0f, 0.0f));
    enemy->SetEnemyToShoot(Personaje);
    //Terminando de crear nivel

    /* Initialize the library */
    if (!glfwInit())
        return -1;

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(800, 800, "Rogue Like", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return -1;
    }


    /* Make the window's context current */
    glfwMakeContextCurrent(window);
    InitGL();
    double ActualTime;
    double LastTime;
    LastTime = glfwGetTime();
    ActualTime = LastTime;
    glfwSetKeyCallback(window, KeyCallback);
    glfwSetCursorPosCallback(window, CursorCallback);
    glfwSetMouseButtonCallback(window, MouseButtonCallback);


    // gameEngine.registerActor(&box);
    //gameEngine.registerActor(&enemy);
    //gameEngine.registerActor(&esfera);
    gameEngine.registerCamera(camara.Camara);
    //gameEngine.registerActor(Personaje);
    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window)) {
        ActualTime = glfwGetTime();
        double DeltaTime = ActualTime - LastTime;
        //cout << DeltaSeconds << endl;
        LastTime = ActualTime;

        int width, height;
        float ratio;
        glfwGetFramebufferSize(window, &width, &height);
        //std::cout << width << ", " << height<< std::endl;
        ratio = width / (float) height;
        glViewport(0, 0, width, height);
        /* Render here */
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);//esto  va antes de render o claculos

        //glfwSetInputMode(window, GLFW_CURSOR, inputSystem.GetAction("pause") ? GLFW_CURSOR_NORMAL : GLFW_CURSOR_DISABLED);
        //glfwSetInputMode(window, GLFW_CURSOR, input.GetInputValues().showMouse ? GLFW_CURSOR_NORMAL : GLFW_CURSOR_DISABLED);
        esfera2->SetActorRelativePosition(esfera2->GetActorRelativePosition() + Vector(10.0f*DeltaTime, 0.0f, 0.0f));

        //gameEngine.CalculateWorldPositions();//esto tal vez deberia ir despues de todo, es decir, desues de los polls, o alguna otra cosa, depsues de procesar los eventos de teclado no lo se, pero tiene sentido que esos eventos se porcesen despues del frame, ya que uno reaccion o hace algo en funcoin de lo que ha visto
        gameEngine.Update(DeltaTime);//actualiza con los eventos obtenidos de la iteracion anteirio, asi como las posiciones globales anteriores, es decir del frame anterior
        gameEngine.PostUpdate();
        gameEngine.render();//una vez hecho cambios renderizo y clculo las posiciones globlaes para la siguiente iteracion, esta
        //Calculos();
        gameEngine.CamRender();

        /* Swap front and back buffers */
        glfwSwapBuffers(window);

        glFlush();
        /* Poll for and process events */
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}