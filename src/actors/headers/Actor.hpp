#pragma once

#include "../../engine/Object.hpp"
#include "../../components/headers/Component.hpp"
#include "../../components/headers/InputComponent.hpp"
#include "../../components/headers/TransformComponent.hpp"
#include "../../engine/headers/GameWorld.hpp"
#include <vector>

//class ComponentManager;
//class GameWorld;

class Actor: public Object {
    private:
        //ComponentManager * CompManager;//al momento de ser creado debo recibir el componentemanager?
    public:
        std::vector<Component *> Components;   
        Actor(GameWorld * InWorld);
        Actor();
        virtual ~Actor();
        GameWorld * World;
        bool PendingKill;
        GameWorld * GetWorld();
        void SetActorRelativePosition(Vector NewPosition);//si no soy hijo de nadie, el relative sera en realidad el world
        Vector GetActorRelativePosition();
        void SetActorRelativeRotation(Rotator NewRotation);
        Rotator GetActorRelativeRotation();
        void SetActorRelativeScale(Vector NewScale);
        Vector GetActorRelativeScale();
        template<class T>
        T * CreateComponent(std::string Nombre);//deberia tener nombre el component!! para poder diferenciar entre componentes del mismo tipo
        template<class T>
        void RegisterComponent(T* Comp);
        void Destroy();
        virtual void ObtainWorldValues();
        virtual void render() override;
        virtual void update(float DeltaTime) override;
        virtual void beginPlay() override;
        virtual void endPlay() override;

        InputComponent* inputComponent;
        TransformComponent * RootComponent;
};

template<class T>
T * Actor::CreateComponent(std::string Nombre){
    T * NuevoComponent = new T; //CompManager->CreateComponent<T>(Nombre);//que lo registre el mundo
    NuevoComponent->Owner = this;
    Components.push_back(NuevoComponent);
    //RegisterComponent<T>(NuevoComponent);
    if(World){
        std::cout << "Intentando registrar el componente" << std::endl;
        World->RegisterComponent<T>(NuevoComponent);
    }
    return NuevoComponent;
}

template<class T>
void Actor::RegisterComponent(T * Comp){
    if(World){
        World->RegisterComponent<T>(Comp);
    }
}