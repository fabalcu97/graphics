#pragma once

#include "./Actor.hpp"
#include "../../components/headers/SphereMeshComponent.hpp"
#include "../../components/headers/BoxCollisionComponent.hpp"
#include "../../components/headers/InputComponent.hpp"
#include "../../structures/headers/Vector.hpp"
#include "./Bullet.hpp"

class Character: public Actor {
  private:

  public:
    BoxCollisionComponent * Collision;
    Character();
    Character(GameWorld * InWorld);
    ~Character();

    float TimeShoot;
    float TimeShootActual;
    int lastshoot;
    InputComponent * Input;
    TransformComponent * PuntoDisparo;
    Vector movement;
    float Velocidad;
    float VidaMaxima;
    float VidaActual;
    void Disparar();
    void RecibirAtaque(float Damage);
    void moveForward(float forward);
    void moveRight(float right);
    void run();
    void stopRun();

    void virtual render() override;
    void virtual update(float DeltaTime) override;
    void virtual beginPlay() override;
    void virtual endPlay() override;
};
