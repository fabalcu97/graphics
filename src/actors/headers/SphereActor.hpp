#pragma once

#include <vector>
#include "./Actor.hpp"
#include "../../components/headers/SphereMeshComponent.hpp"
#include "../../components/headers/BoxCollisionComponent.hpp"
#include "../../structures/headers/Vector.hpp"

class SphereActor: public Actor {
  private:

  public:
    BoxCollisionComponent * Colision;
    SphereActor();
    SphereActor(GameWorld * InWorld);
    ~SphereActor();

    void virtual render() override;
    void virtual update(float DeltaTime) override;
    void virtual beginPlay() override;
    void virtual endPlay() override;
};