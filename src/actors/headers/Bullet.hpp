#pragma once

#include "./Actor.hpp"
#include "../../components/headers/SphereMeshComponent.hpp"
#include "../../components/headers/BoxCollisionComponent.hpp"
#include "../../structures/headers/Vector.hpp"

class Bullet: public Actor {
  private:

  public:
    BoxCollisionComponent * Colision;
    Bullet();
    Bullet(GameWorld * InWorld);
    virtual ~Bullet();

    Vector Direccion;
    bool Disparado;
    float Velocidad;
    float Damage;
    void Disparar();
    void RecibirAtaque();

    //void BeginOverlap();
    void BeginOverlap(CollisionComponent * OtherComp, Actor * OtherActor);
    void EndOverlap(CollisionComponent * OtherComp, Actor * OtherActor);

    void virtual render() override;
    void virtual update(float DeltaTime) override;
    void virtual beginPlay() override;
    void virtual endPlay() override;
};
