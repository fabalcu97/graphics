#pragma once

#include "./Actor.hpp"
#include "./EnemyBullet.hpp"
#include "../../components/headers/BoxMeshComponent.hpp"
#include "../../components/headers/BoxCollisionComponent.hpp"
#include "../../structures/headers/Vector.hpp"

class Enemy : public Actor {
  private:

  public:
    BoxCollisionComponent * Collision;
    Enemy();
    Enemy(GameWorld * InWorld);
    ~Enemy();

    TransformComponent * PuntoDisparo;
    Actor* EnemyToShoot;
    float Velocidad;
    float VidaMaxima; 
    float VidaActual; 
    float ShouldShoot;
    void Disparar();
    void SetEnemyToShoot(Actor* enemy);
    void RecibirAtaque(float Damage);

    void virtual render() override;
    void virtual update(float DeltaTime) override;
    void virtual beginPlay() override;
    void virtual endPlay() override;
};

