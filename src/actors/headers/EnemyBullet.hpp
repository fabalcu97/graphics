
#pragma once

#include "./Actor.hpp"
#include "../../components/headers/SphereMeshComponent.hpp"
#include "../../components/headers/BoxCollisionComponent.hpp"
#include "../../structures/headers/Vector.hpp"

class EnemyBullet: public Actor {
  private:

  public:
    BoxCollisionComponent * Colision;
    EnemyBullet();
    EnemyBullet(GameWorld * InWorld);
    ~EnemyBullet();

    bool Disparado;
    float Velocidad = 20;
    float Damage;
    Vector Direction;
    void RecibirAtaque();

    void virtual render() override;
    void virtual update(float DeltaTime) override;
    void virtual beginPlay() override;
    void virtual endPlay() override;
    void Disparar();
};