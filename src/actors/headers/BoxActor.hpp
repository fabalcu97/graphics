#pragma once

#include <vector>
#include <iostream>
#include "./Actor.hpp"
#include "../../components/headers/BoxMeshComponent.hpp"
#include "../../components/headers/BoxCollisionComponent.hpp"
#include "../../structures/headers/Vector.hpp"

class BoxActor: public Actor {
  private:

  public:
    float MovementSpeed;
    BoxCollisionComponent * Collision;
    BoxActor(GameWorld * InWorld);
    BoxActor();
    ~BoxActor();

    void virtual render() override;
    void virtual update(float DeltaTime) override;
    void virtual beginPlay() override;
    void virtual endPlay() override;
};