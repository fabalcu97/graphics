#pragma once

#include "./Actor.hpp"
#include "../../components/headers/CameraComponent.hpp"

class CameraActor: public Actor {
private:
public:
    CameraComponent * Camara;
    CameraActor();
    ~CameraActor();
};
