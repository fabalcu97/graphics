#include "../headers/Enemy.hpp"

Enemy::Enemy(GameWorld * InWorld) {
    World = InWorld;
    RootComponent = new BoxMeshComponent;
    RootComponent->Scale = Vector(2.0f, 2.0f, 2.0f);
    Collision = CreateComponent<BoxCollisionComponent>("Colision");
    Collision->AttachToComponent(RootComponent);
    Collision->SetRelativePosition(Vector(0.0f, 0.0f, 0.0f));
    PuntoDisparo = new TransformComponent;
    PuntoDisparo->SetRelativePosition(Vector(5.0f, 0.0f, 0.0f));
    PuntoDisparo->AttachToComponent(RootComponent);
    ShouldShoot = 0;
    std::cout << "Construyendo Enemy Actor" << std::endl;
    EnemyToShoot = nullptr;
}

Enemy::Enemy() {
    World = nullptr;
    RootComponent = new BoxMeshComponent;
    RootComponent->Scale = Vector(2.0f, 2.0f, 2.0f);
    Collision = CreateComponent<BoxCollisionComponent>("Colision");
    ShouldShoot = 0;
    EnemyToShoot = nullptr;
}

Enemy::~Enemy() {
    //delete RootComponent;
}

void Enemy::render () {
    //if(RootComponent){
        RootComponent->render();//este es la base de mi espacio
    //}
    //aqui aplicar las traslaciones del root component
}

void Enemy::update (float DeltaTime) {
    ShouldShoot += DeltaTime;
    // std::cout << ShouldShoot << std::endl;
    if (ShouldShoot > 4) {
        ShouldShoot = 0;
        Disparar();
    }
}

void Enemy::beginPlay () {
}

void Enemy::endPlay () {
}


void Enemy::Disparar(){
    Vector direction;
    if (EnemyToShoot) {
        direction = EnemyToShoot->GetActorRelativePosition() - GetActorRelativePosition();
    } else {
        direction = PuntoDisparo->GetGlobalPosition() - GetActorRelativePosition();
    }
    EnemyBullet* bullet = World->CreateActor<EnemyBullet>();
    bullet->SetActorRelativePosition(PuntoDisparo->GetGlobalPosition());
    bullet->Direction = direction.Normalize();
    bullet->Disparar();
}
void Enemy::RecibirAtaque(float Damage){

}
void Enemy::SetEnemyToShoot (Actor* enemy) {
    EnemyToShoot = enemy;
}