#include "../headers/SphereActor.hpp"

SphereActor::SphereActor(GameWorld * InWorld) {
    World = InWorld;
    RootComponent = new SphereMeshComponent;
    RootComponent->Scale = Vector(2.0f, 2.0f, 2.0f);
    Colision = CreateComponent<BoxCollisionComponent>("Colision");
    Colision->AttachToComponent(RootComponent);
    Colision->SetRelativePosition(Vector(0.0f, 0.0f, 0.0f));
    std::cout << "Construyendo Sphere Actor" << std::endl;
}

SphereActor::SphereActor() {
    World = nullptr;
    RootComponent = new SphereMeshComponent;
    RootComponent->Scale = Vector(2.0f, 2.0f, 2.0f);
    Colision = CreateComponent<BoxCollisionComponent>("Colision");
}

SphereActor::~SphereActor() {
    //delete RootComponent;
}

void SphereActor::render () {
    //if(RootComponent){
        RootComponent->render();//este es la base de mi espacio
    //}
    //aqui aplicar las traslaciones del root component
}

void SphereActor::update (float DeltaTime) {
}

void SphereActor::beginPlay () {
}

void SphereActor::endPlay () {
}