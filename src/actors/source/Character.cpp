#include "../headers/Character.hpp"

Character::Character(GameWorld * InWorld) {
    World = InWorld;
    RootComponent = new SphereMeshComponent;
    RootComponent->Scale = Vector(2.0f, 2.0f, 2.0f);
    Collision = CreateComponent<BoxCollisionComponent>("Colision");
    Collision->AttachToComponent(RootComponent);
    Collision->SetRelativePosition(Vector(0.0f, 0.0f, 0.0f));
    PuntoDisparo = new TransformComponent;
    PuntoDisparo->AttachToComponent(RootComponent);
    PuntoDisparo->SetRelativePosition(Vector(5.0f, 0.0f, 0.0f));
    Input = CreateComponent<InputComponent>("Input");
    Input->BindAxis("moveForward", this, &Character::moveForward);
    Input->BindAxis("moveRight", this, &Character::moveRight);
    Input->BindAction("run", PRESS, this, &Character::run);
    Input->BindAction("run", RELEASE, this, &Character::stopRun);
    Input->BindAction("shoot", PRESS, this, &Character::Disparar);
    //Input = new InputComponent;
    //Input->Owner = this;
    std::cout << "Construyendo Character Actor" << std::endl;

    TimeShoot = 2.0f;
    TimeShootActual = 0.0f;

    Velocidad = 10.0f;
    movement = Vector(0, 0, 0);
}

Character::Character() {
    World = nullptr;
    RootComponent = new SphereMeshComponent;
    RootComponent->Scale = Vector(2.0f, 2.0f, 2.0f);
    Collision = CreateComponent<BoxCollisionComponent>("Colision");
}

Character::~Character() {
    //delete RootComponent;
}

void Character::moveForward (float axisValue) {
    movement.X = axisValue;
}

void Character::moveRight (float axisValue) {
    movement.Z = axisValue;
}

void Character::run () {
    Velocidad = 20.0f;
}

void Character::stopRun () {
    Velocidad = 10.0f;
}

void Character::render () {
    //if(RootComponent){
        RootComponent->render();//este es la base de mi espacio
    //}
    //aqui aplicar las traslaciones del root component
}

void Character::update (float DeltaTime) {
    //if(movement.X != 0.0f && movement.Y != 0.0f && movement.Z != 0.0f)
    Vector Desplazamiento = movement.Normalize() * Velocidad * DeltaTime;
    SetActorRelativePosition( GetActorRelativePosition() + Desplazamiento); 
    /*std::cout << Desplazamiento.X << ", " << Desplazamiento.Y << ", " << Desplazamiento.Z << std::endl;
    SetActorRelativePosition( GetActorRelativePosition() + Vector(
            movement.X*Velocidad*DeltaTime,
            0.0f,
            movement.Z*Velocidad*DeltaTime
        )
    );*/

    //int shoot = Input->GetAction("run");
    /*TimeShootActual += DeltaTime;
    if(TimeShootActual >= TimeShoot){
        Disparar();
        TimeShootActual = 0.0f;
    }*/
    //lastshoot = shoot;
}

void Character::beginPlay () {
}

void Character::endPlay () {
}

void Character::Disparar(){
    Bullet * Bala = World->CreateActor<Bullet>();
    Bala->SetActorRelativePosition(PuntoDisparo->GetGlobalPosition());
    Bala->Direccion = (PuntoDisparo->GetGlobalPosition() - GetActorRelativePosition()).Normalize();
    Bala->Disparar();
}
void Character::RecibirAtaque(float Damage){

}