#include "../headers/BoxActor.hpp"

BoxActor::BoxActor(GameWorld * InWorld) {
    World = InWorld;
    RootComponent = new BoxMeshComponent;
    RootComponent->Scale = Vector(2.0f, 2.0f, 2.0f);
    RootComponent->Position = Vector(10.0f, 10.0f, 2.0f);
    Collision = CreateComponent<BoxCollisionComponent>("Collision");
    Collision->AttachToComponent(RootComponent);
    Collision->SetRelativePosition(Vector(0.0f, 0.0f, 0.0f));
    std::cout << "Construyendo Box Actor" << std::endl;
    MovementSpeed = 45.0f;
}

BoxActor::BoxActor() {
    RootComponent = new BoxMeshComponent;
    RootComponent->Scale = Vector(2.0f, 2.0f, 2.0f);
    RootComponent->Position = Vector(10.0f, 10.0f, 2.0f);
    Collision = CreateComponent<BoxCollisionComponent>("Collision");
    MovementSpeed = 45.0f;
}

BoxActor::~BoxActor() {
    //delete RootComponent;
}

void BoxActor::render () {
    //if(RootComponent){
        RootComponent->render();//este es la base de mi espacio
    //}
    //aqui aplicar las traslaciones del root component
}

void BoxActor::update (float DeltaTime) {
    // float forward = inputComponent->GetAxis("moveForward");
    // float right = inputComponent->GetAxis("moveRight");
    float spaceToMove = DeltaTime * MovementSpeed;

    // if (right == 1) {
    //     RootComponent->Position.X = RootComponent->Position.X - spaceToMove;
    // }
    // else if (right == -1) {
    //     RootComponent->Position.X = RootComponent->Position.X + spaceToMove;
    // }
    // if (forward == 1) {
    //     RootComponent->Position.Z = RootComponent->Position.Z - spaceToMove;
    // }
    // else if (forward == -1) {
    //     RootComponent->Position.Z = RootComponent->Position.Z + spaceToMove;
    // }
}

void BoxActor::beginPlay () {
}

void BoxActor::endPlay () {
}