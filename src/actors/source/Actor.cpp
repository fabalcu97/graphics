#include "../headers/Actor.hpp"
//#include "../../engine/headers/GameWorld.hpp"

Actor::Actor (GameWorld * InWorld) {
    RootComponent = nullptr;
    inputComponent = nullptr;
    World = InWorld;
    PendingKill = false;
}

Actor::Actor () {
    
}

Actor::~Actor () {
    /*if(RootComponent){
        delete RootComponent;
    }
    if(inputComponent){
        delete inputComponent;
    }*/
}

void Actor::ObtainWorldValues () {
    if(RootComponent){
        RootComponent->ObtainWorldValues();//este es la base de mi espacio
    }
}

void Actor::render () {
    if(RootComponent){
        RootComponent->render();//este es la base de mi espacio
    }
}

void Actor::update (float DeltaTime) {

}

void Actor::beginPlay () {

}

void Actor::endPlay () {

}

GameWorld * Actor::GetWorld(){
    return World;
}

void Actor::SetActorRelativePosition(Vector NewPosition){//si no soy hijo de nadie, el relative sera en realidad el world
    RootComponent->SetRelativePosition(NewPosition);
}
Vector Actor::GetActorRelativePosition(){
    return RootComponent->GetRelativePosition();
}
void Actor::SetActorRelativeRotation(Rotator NewRotation){
    RootComponent->SetRelativeRotation(NewRotation);
}
Rotator Actor::GetActorRelativeRotation(){
    return RootComponent->GetRelativeRotation();
}
void Actor::SetActorRelativeScale(Vector NewScale){
    RootComponent->SetRelativeScale(NewScale);
}
Vector Actor::GetActorRelativeScale(){
    return RootComponent->GetRelativeScale();

}

void Actor::Destroy(){
    PendingKill = true;
    /*for(int i = 0; i < Components.size(); i++){//deberia ser polimorfico
        Components[i]->Destroy();
        delete Components[i];
        //World->UnregisterComponent(Components[i]);
    }*/
    //si hago eso despues?
    //decirle a world que los des registre, 
    //decirle al world que voy a morir
    World->EnqueueToKill(this);

}