#include "../headers/Bullet.hpp"

Bullet::Bullet(GameWorld * InWorld) {
    World = InWorld;
    RootComponent = new SphereMeshComponent;
    RootComponent->Scale = Vector(0.5f, 0.5f, 0.5f);
    Colision = CreateComponent<BoxCollisionComponent>("Colision");
    Colision->AttachToComponent(RootComponent);
    Colision->SetRelativePosition(Vector(0.0f, 0.0f, 0.0f));
    //Colision->AddCallback(this, &Bullet::BeginOverlap);
    Colision->BindBeginOverlap(this, &Bullet::BeginOverlap);
    Colision->BindEndOverlap(this, &Bullet::EndOverlap);
    std::cout << "Construyendo Sphere Actor" << std::endl;
    Disparado = false;
    Velocidad = 20.0f;
}

Bullet::Bullet() {
    World = nullptr;
    RootComponent = new SphereMeshComponent;
    RootComponent->Scale = Vector(2.0f, 2.0f, 2.0f);
    Colision = CreateComponent<BoxCollisionComponent>("Colision");
    Velocidad = 20.0f;
}

Bullet::~Bullet() {
    //delete RootComponent;
}

void Bullet::render () {
    //if(RootComponent){
        RootComponent->render();//este es la base de mi espacio
    //}
    //aqui aplicar las traslaciones del root component
}

void Bullet::update (float DeltaTime) {
    if(Disparado){
        //std::cout << "avanzando" << std::endl;
        SetActorRelativePosition(GetActorRelativePosition() + Vector(Direccion.X*Velocidad*DeltaTime, 0.0f, Direccion.Z*Velocidad*DeltaTime));
    }
}

void Bullet::beginPlay () {
}

void Bullet::endPlay () {
}


void Bullet::RecibirAtaque(){

}

void Bullet::Disparar(){
    Disparado = true;
}

void Bullet::BeginOverlap(CollisionComponent * OtherComp, Actor * OtherActor){
    std::cout << "Bullet begin overlpa" << std::endl;
    Destroy();
}

void Bullet::EndOverlap(CollisionComponent * OtherComp, Actor * OtherActor){
    std::cout << "Bullet end overlap" << std::endl;
}