#include "../headers/EnemyBullet.hpp"

EnemyBullet::EnemyBullet(GameWorld * InWorld) {
    World = InWorld;
    RootComponent = new SphereMeshComponent;
    RootComponent->Scale = Vector(0.5f, 0.5f, 0.5f);
    Colision = CreateComponent<BoxCollisionComponent>("Colision");
    Colision->AttachToComponent(RootComponent);
    Colision->SetRelativePosition(Vector(0.0f, 0.0f, 0.0f));
    std::cout << "Construyendo Sphere Actor" << std::endl;
    Direction = Vector(1.0f, 0.0f, 0.0f);
}

EnemyBullet::EnemyBullet() {
    World = nullptr;
    RootComponent = new SphereMeshComponent;
    RootComponent->Scale = Vector(2.0f, 2.0f, 2.0f);
    Colision = CreateComponent<BoxCollisionComponent>("Colision");
}

EnemyBullet::~EnemyBullet() {
    //delete RootComponent;
}

void EnemyBullet::render () {
    //if(RootComponent){
        RootComponent->render();//este es la base de mi espacio
    //}
    //aqui aplicar las traslaciones del root component
}

void EnemyBullet::update (float DeltaTime) {
    if (Disparado) {
        Vector position = GetActorRelativePosition();
        SetActorRelativePosition(Vector(
            position.X + (Direction.X * DeltaTime * Velocidad),
            position.Y,
            position.Z + (Direction.Z * DeltaTime * Velocidad)
        ));
    }
}

void EnemyBullet::beginPlay () {
}

void EnemyBullet::endPlay () {
}


void EnemyBullet::RecibirAtaque(){

}

void EnemyBullet::Disparar () {
    Disparado = true;
}