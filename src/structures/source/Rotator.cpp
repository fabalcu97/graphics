#include "../headers/Rotator.hpp"

Rotator::Rotator(float inPitch, float inYaw, float inRoll){
    Pitch = inPitch;
    Yaw = inYaw;
    Roll = inRoll;
}

Rotator::Rotator () {
    Pitch = 0.0f;
    Yaw = 0.0f;
    Roll = 0.0f;
}

Rotator::~Rotator () {

}

