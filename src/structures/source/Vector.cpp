#include "../headers/Vector.hpp"

#ifdef __APPLE__
    #include <cmath>
#elif __linux__
    #include <math.h>
#endif

Vector Vector::operator + (const Vector & v){
    return Vector(X + v.X, Y + v.Y, Z + v.Z);
}

Vector Vector::operator - (const Vector & v){
    return Vector(X - v.X, Y - v.Y, Z - v.Z);
}

Vector Vector::operator * (int n){
    return Vector(X*n, Y*n, Z*n);
}

Vector Vector::operator / (int n){
    return Vector(X/n, Y/n, Z/n);
}

Vector Vector::operator * (float n){
    return Vector(X*n, Y*n, Z*n);
}

Vector Vector::operator / (float n){
    return Vector(X/n, Y/n, Z/n);
}

bool Vector::operator == (Vector & v){
    return (X == v.X) && (Y == v.Y) && (Z == v.Z);
}

Vector Vector::CrossProduct (Vector & v){
    return Vector(Y*v.Z - Z*v.Y, Z*v.X - X*v.Z, X*v.Y - Y*v.X);
}
float Vector::DotProduct (Vector & v){
    return X*v.X + Y*v.Y + Z*v.Z;
}


float Vector::Magnitud(){
    return std::sqrt(X*X + Y*Y + Z*Z);
}

Vector Vector::Normalize(){
    float M = Magnitud();
    if(M){
        return Vector(X/M, Y/M, Z/M);
    }
    return Vector(0, 0, 0);
}

Vector::Vector (float inX, float inY, float inZ): X(inX), Y(inY), Z(inZ) {

}

Vector::Vector () {
    X = 0.0f;
    Y = 0.0f;
    Z = 0.0f;
}

Vector::~Vector () {

}

