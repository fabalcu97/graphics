#include "../headers/Triangle.hpp"

Triangle::Triangle() {

}

Triangle::Triangle(int a, int b, int c, int niv, bool ori): IdA(a), IdB(b), IdC(c), Nivel(niv), Orientacion(ori) {

}

Triangle::Triangle(Vector va, Vector vb, Vector  vc, int niv, bool ori): A(va), B(vb), C(vc), Nivel(niv), Orientacion(ori) {

}

Triangle::Triangle(Vector va, Vector vb, Vector  vc, int a, int b, int c): A(va), B(vb), C(vc), IdA(a), IdB(b), IdC(c) {

}
Triangle::Triangle(Vector va, Vector vb, Vector  vc, int a, int b, int c, int niv, bool ori): A(va), B(vb), C(vc), IdA(a), IdB(b), IdC(c), Nivel(niv), Orientacion(ori) {

}