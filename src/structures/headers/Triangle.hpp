#pragma once

#include "Vector.hpp"

class Triangle {
    public:
        int IdA;
        int IdB;
        int IdC;
        int Nivel;
        bool Orientacion;// 0 ahcia arriga, 1 hacia abajo, esto no se si lo use
        Vector Centro;
        Vector A;
        Vector B;
        Vector C;
        float Phi;
        float Theta;
        Triangle();
        Triangle(int a, int b, int c, int niv, bool ori);
        Triangle(Vector va, Vector vb, Vector  vc, int niv, bool ori);
        Triangle(Vector va, Vector vb, Vector  vc, int a, int b, int c);
        Triangle(Vector va, Vector vb, Vector  vc, int a, int b, int c, int niv, bool ori);
};
