#pragma once

class Vector {
  private:
  public:
    float X;
    float Y;
    float Z;
    Vector operator + (const Vector & v);
    Vector operator - (const Vector & v);
    Vector operator * (int n);
    Vector operator / (int n);
    Vector operator * (float n);
    Vector operator / (float n);
    bool operator == (Vector & v);
    Vector CrossProduct (Vector & v);
    float DotProduct (Vector & v);

    float Magnitud();
    Vector Normalize();
    Vector(float inX, float inY, float inZ);
    Vector();
    ~Vector();
};