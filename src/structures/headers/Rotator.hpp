#pragma once

class Rotator {
  private:
  public:
    float Pitch;
    float Yaw;
    float Roll;
    Rotator(float inPitch, float inYaw, float inRoll);
    Rotator();
    ~Rotator();
};