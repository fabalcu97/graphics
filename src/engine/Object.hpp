#pragma once

#include <string>
#include <iostream>

class Object {
  private:
    std::string Name;

  public:
    Object * Owner;
    virtual std::string GetName();
    virtual void LogMessage(std::string Message);
    virtual void SetName(std::string NewName);
    virtual void update(float DeltaTime) = 0;
    virtual void beginPlay() = 0;
    virtual void endPlay() = 0;
    virtual void render() = 0;
    Object();
    virtual ~Object();
};