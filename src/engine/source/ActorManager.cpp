#include "../headers/ActorManager.hpp"
#include "../../actors/headers/Actor.hpp"
#include "../../components/headers/Component.hpp"

ActorManager::ActorManager() {
}

ActorManager::~ActorManager() {
}

void ActorManager::Render(){
    for(int i = 0; i < ActorsArray.size(); i++){
        ActorsArray[i]->render();
    }
}

void ActorManager::Update(float DeltaTime){
    for(int i = 0; i < ActorsArray.size(); i++){
        ActorsArray[i]->update(DeltaTime);
    }
}


void ActorManager::KillActors() {
    for(int i = 0; i < ActorsToKill.size(); i++){
        for(int j = 0; j < ActorsToKill[i]->Components.size(); j++){//deberia ser polimorfico
            ActorsToKill[i]->Components[j]->Destroy();
            cout << "ActorManager: Llamando al delete del componente " << j << endl;
            delete (ActorsToKill[i]->Components[j]);
            cout << "ActorManager: delete llamado del componente " << j << endl;
            //World->UnregisterComponent(Components[i]);
        }
        cout << "ActorManager: Llamando al delete del actor " << i << endl;
        delete ActorsToKill[i];
        cout << "ActorManager: delete llamado del actor " << i << endl;
    }
    cout << "ActorManager: limpiando array de actores to kill" << endl;
    ActorsToKill.clear();
}