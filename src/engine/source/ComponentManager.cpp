#include "../headers/ComponentManager.hpp"

ComponentManager::ComponentManager() {
}

ComponentManager::~ComponentManager() {
}

void ComponentManager::Update(float DeltaTime){
    for(int i = 0; i < ComponentsArray.size(); i++){
        ComponentsArray[i]->update(DeltaTime);
    }
}