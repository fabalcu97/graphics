#include "../headers/GameWorld.hpp"
#include "../headers/GameEngine.hpp"
#include "../../actors/headers/Actor.hpp"
//#include "../headers/ComponentManager.hpp"

GameWorld::GameWorld() {
    std::cout << "World created" << std::endl;
    CollisionSys = new CollisionSystem;
}

GameWorld::~GameWorld() {
}

void GameWorld::render () {

}

void GameWorld::update (float DeltaTime) {
    CollisionSys->CheckOverlaps();

}

void GameWorld::beginPlay () {

}

void GameWorld::endPlay () {

}

void GameWorld::RegisterActor(Actor * actor){
    Engine->registerActor(actor);
}


/*template<>
void GameWorld::RegisterComponent<BoxCollisionComponent>(BoxCollisionComponent * Comp){
    Engine->CompManager->RegisterComponent<BoxCollisionComponent>(Comp);
    CollisionSys->AddBoxCollision(Comp);
}*/

void GameWorld::EnqueueToKill(Actor * actor){
    Engine->ActorsManager->EnqueueToKill(actor);
}

void GameWorld::KillActors(){//al final del la iteraciones o updates
    //hacer deletes, o el engine puede hacerlo directamente

}

void GameWorld::LogMessage(std::string Message){
    std::cout << "GameWorld: " << Message << std::endl;
}
