#include "../headers/GameEngine.hpp"
#include "../headers/GameWorld.hpp"
#include "../../actors/headers/Actor.hpp"

GameEngine::GameEngine() {
	ActualCamera = nullptr;
	ComponentsManager = new ComponentManager;
	ActorsManager = new ActorManager;
	World = new GameWorld;//este se deberi crear al inicio del juego, deberia tener una funcion create world
	World->Engine = this;
}

GameEngine::~GameEngine() {
	delete ComponentsManager;
	delete ActorsManager;
	//delete World;
}


GameWorld * GameEngine::GetWorld(){
	return World;
}

void GameEngine::CalculateWorldPositions() {
	glMatrixMode(GL_MODELVIEW);//entrando a la matriz del model view
	glLoadIdentity();//reseteando todo,
	glBegin(GL_LINES);
	glColor3d(255,0,0);
	glVertex3d(0, 0, 0);
	glVertex3d(5, 0, 0);
	glColor3d(0, 255, 0);
	glVertex3d(0, 0, 0);
	glVertex3d(0, 5, 0);
	glColor3d(0, 0, 255);
	glVertex3d(0, 0, 0);
	glVertex3d(0, 0, 5);
	glEnd();
	for(int i = 0; i < list.size(); i++){
		list[i]->ObtainWorldValues();
	}
}

void GameEngine::render() {
	cout << "GameEngine: Iniciando Render" << endl;
	glMatrixMode(GL_MODELVIEW);//entrando a la matriz del model view
	glLoadIdentity();//reseteando todo,
	glBegin(GL_LINES);
	glColor3d(255,0,0);
	glVertex3d(0, 0, 0);
	glVertex3d(5, 0, 0);
	glColor3d(0, 255, 0);
	glVertex3d(0, 0, 0);
	glVertex3d(0, 5, 0);
	glColor3d(0, 0, 255);
	glVertex3d(0, 0, 0);
	glVertex3d(0, 0, 5);
	glEnd();

	ActorsManager->Render();
	/*for(int i = 0; i < list.size(); i++){
		list[i]->render();
	}*/
    /*for (std::vector<Actor*>::iterator it = list.begin(); it != list.end(); it++) {
        (*it)->render();
    }*/
    // glBegin(GL_LINE_LOOP);
    // glVertex2d(0, 0);
    // glVertex2d(0, 1);
    // glVertex2d(1, 0);
    // glEnd();
	cout << "GameEngine: Finalizando Render" << endl;
}

void GameEngine::CamRender() {
	cout << "GameEngine: Iniciando CamRender" << endl;
	glMatrixMode(GL_PROJECTION);//entrando en modo projection
	glLoadIdentity();//cargando la identidad
    //aqui se agregand instrucciones al projecyion
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f); //(R, G, B, transparencia) en este caso un fondo negro
	if(ActualCamera){
		ActualCamera->CamRender();
	}
	else{
		gluPerspective(90, 1, 1, 200.0f);
		gluLookAt(20.0f, 20.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);//esto va al inicio de todos los calculos o llamadas paar dibujar algo
		//glOrtho(-50.0f,  50.0f, -50.0f, 50.0f, -50.0f, 50.0f); //usaremos una proyeccion oroto grafica, que va de -50 a 50 en ambos ejes
	}
	cout << "GameEngine: Finalizando CamRender" << endl;
}

void GameEngine::registerActor(Actor * a) {
    //list.push_back(a);
	ActorsManager->RegisterActor(a);
}

void GameEngine::registerCamera(CameraComponent * camera) {
	ActualCamera = camera;
}

void GameEngine::Update(float DeltaTime){
	cout << "GameEngine: Iniciando Update" << endl;
	ComponentsManager->Update(DeltaTime);//estos dos deberian estar dentro del update de world,
	ActorsManager->Update(DeltaTime);
	World->update(DeltaTime);//el deberia actualizar a los actores, al componente manager, ya que el se enarga de los actores del mundo y la escena
	/*for(int i = 0; i < list.size(); i++){
		list[i]->update(DeltaTime);
	}*/
	cout << "GameEngine: Finalizando Update" << endl;
}

void GameEngine::PostUpdate(){
	cout << "GameEngine: Inciando PostUpdate" << endl;
	ActorsManager->KillActors();
	//World->KillActors();
	cout << "GameEngine: Finalizando PostUpdate" << endl;
}

void GameEngine::LogMessage(std::string Message){
    std::cout << "GameEngine: " << Message << std::endl;
}