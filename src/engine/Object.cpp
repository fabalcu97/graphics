#include "Object.hpp"

Object::Object(){

}

Object::~Object(){

}

std::string Object::GetName(){
    return Name;
}

void Object::SetName(std::string NewName){
    Name = NewName;
}

void Object::LogMessage(std::string Message){
    std::cout << "Object: " << Message << std::endl;
}