#pragma once
#ifdef __APPLE__
    #include <OpenGL/glu.h>
#elif __linux__
    #include <GLU/glu.h>
#endif
#include <GLFW/glfw3.h>
#include <vector>
#include <iostream>
#include <type_traits>
//#include "GameWorld.hpp"
#include "ComponentManager.hpp"
#include "ActorManager.hpp"
//#include "../../actors/headers/Actor.hpp"
#include "../../components/headers/CameraComponent.hpp"

class GameWorld;
class Actor;

class GameEngine {
    private:
        GameWorld * World;
        CameraComponent * ActualCamera;
    public:
        ActorManager * ActorsManager;
        ComponentManager * ComponentsManager;
        GameWorld * GetWorld();
        std::vector<Actor *> list;
        virtual void LogMessage(std::string Message);
        void CalculateWorldPositions();
        void render();
        void CamRender();
        void registerActor(Actor * a);
        void registerCamera(CameraComponent * camera);
        void Update(float DeltaTime);
        void PostUpdate();
        GameEngine();
        ~GameEngine();
};