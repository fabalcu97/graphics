#pragma once

#include <vector>
#include <string>
#include <iostream>
//#include "../../actors/headers/Actor.hpp"

class Actor;

using namespace std;

class ActorManager {
    private:
        vector<Actor *> ActorsArray;
        vector<Actor *> ActorsToKill;
    public:
        void Render();
        void Update(float DeltaTime);
        template<typename T>
        void RegisterActor(T * Actor);
        template<typename T>
        void EnqueueToKill(T * Actor);
        void KillActors();
        ActorManager();
        ~ActorManager();
};

template<typename T>
void ActorManager::RegisterActor(T * Actor){
    ActorsArray.push_back(Actor);
}

template<typename T>
void ActorManager::EnqueueToKill(T * actor){
    vector<Actor *>::iterator it;
    it = find(ActorsArray.begin(), ActorsArray.end(), actor);
    if(it != ActorsArray.end()){
        ActorsToKill.push_back(actor);
        ActorsArray.erase(it);
    }
    //buscar
    //eliminar
    //destruir complementos
    //delete
}