#pragma once

#include <type_traits>
#include <vector>
#include <string>
#include <iostream>
#include "../../systems/headers/CollisionSystem.hpp"
#include "../../systems/headers/InputSystem.hpp"
#include "../../systems/headers/RenderSystem.hpp"

class ComponentManager {
    private:
        std::vector<Component *> ComponentsArray;
    public:
        template<typename T>
        T * CreateComponent(std::string Nombre);//deberia tener nombre el component!! para poder diferenciar entre componentes del mismo tipo
        template<typename T>
        void RegisterComponent(T * Comp);//deberia tener nombre el component!! para poder diferenciar entre componentes del mismo tipo
        template<typename T>
        void UnregisterComponent(T * Comp);//deberia tener nombre el component!! para poder diferenciar entre componentes del mismo tipo
        void Update(float DeltaTime);
        ComponentManager();
        ~ComponentManager();
};

template<typename T>
T * ComponentManager::CreateComponent(std::string Nombre){
    T * NuevoComponent = new T;
    NuevoComponent->SetName(Nombre);
    //establecer el owner lo debe hacer la funion que crea el actor
    return NuevoComponent;
}

template <typename T>
void ComponentManager::RegisterComponent(T * Comp){
    ComponentsArray.push_back(Comp);
}

template <typename T>
void ComponentManager::UnregisterComponent(T * Comp){
    vector<Component *>::iterator it;
    it = find(ComponentsArray.begin(), ComponentsArray.end(), Comp);
    if(it != ComponentsArray.end()){
        ComponentsArray.erase(it);
    }
}

/*template <>
void ComponentManager::RegisterComponent<BoxCollisionComponent>(BoxCollisionComponent * Comp){
    //registrarlo al collsision system
    //deberia verificar que el componente no este resgistrado para evitar duplicados
    std::cout << "Hola" << std::endl;
    ComponentsArray.push_back(Comp);
}*/