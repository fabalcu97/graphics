#pragma once

#include <type_traits>
#include <typeinfo>
#include <utility>
#include "../Object.hpp"
#include "../../structures/headers/Vector.hpp"
#include "../../engine/headers/GameEngine.hpp"//no estaba esto
#include "../../systems/headers/CollisionSystem.hpp"
#include "../../systems/headers/InputSystem.hpp"
#include "../../components/headers/InputComponent.hpp"

//necesito comunicarme con el actor manager, por ahora sera directo al game engine y su lista de actores

//class GameEngine;
class Actor;

class GameWorld: public Object {
private:
    CollisionSystem * CollisionSys;
public:
    InputSystem * InputSys;//por ahora lo recibire, pero deberia estar de otra forma
    GameEngine * Engine;
    void RegisterActor(Actor * actor);
    virtual void LogMessage(std::string Message) override;
    void render() ;
    void update(float DeltaTime) ;
    void beginPlay() ;
    void endPlay() ;
    template<typename T>
    T * CreateActor();//deberia tener nombre el component!! para poder diferenciar entre componentes del mismo tipo
    template<typename T>
    void RegisterComponent(T * Comp){
        Comp->World = this;
        Engine->ComponentsManager->RegisterComponent<T>(Comp);
    }
    template<typename T>
    void UnregisterComponent(T * Comp){
        std::cout << "Unregistrando componente" << std::endl;
        Engine->ComponentsManager->UnregisterComponent<T>(Comp);
    }
    void EnqueueToKill(Actor * actor);
    void KillActors();
    GameWorld();
    ~GameWorld();
};

template<typename T>
T * GameWorld::CreateActor(){
    T * NewActor = new T (this); //CompManager->CreateComponent<T>(Nombre);//que lo registre el mundo
    NewActor->Owner = this;//el propietario sera el mundo, o tal vez sea el nivel
    RegisterActor(NewActor);
    //NewActor->World = this;
    return NewActor;
}

template<>
inline void GameWorld::RegisterComponent<BoxCollisionComponent>(BoxCollisionComponent * Comp){
    Comp->World = this;
    Engine->ComponentsManager->RegisterComponent<BoxCollisionComponent>(Comp);
    std::cout << "Registrando BoxCollision" << std::endl;
    CollisionSys->AddBoxCollision(Comp);
}

template<>
inline void GameWorld::RegisterComponent<InputComponent>(InputComponent * Comp){
    Comp->World = this;
    Engine->ComponentsManager->RegisterComponent<InputComponent>(Comp);
    std::cout << "Registrando InputComponent" << std::endl;
    Comp->SetInputSystem(InputSys);
}

template<>
inline void GameWorld::UnregisterComponent<BoxCollisionComponent>(BoxCollisionComponent * Comp){
    Engine->ComponentsManager->UnregisterComponent<BoxCollisionComponent>(Comp);
    std::cout << "Unregister BoxCollision" << std::endl;
    CollisionSys->RemoveBoxCollision(Comp);
    std::cout << "Terminado el unregister BoxCollision" << std::endl;
}