#pragma once

#include "../Object.hpp"

class GameMode: public Object{
private:
public:
    void virtual render() override;
    void virtual update(float DeltaTime) override;
    void virtual beginPlay() override;
    void virtual endPlay() override;
    GameMode();
    ~GameMode();
};

