#include <GLFW/glfw3.h>
#include <iostream>
#include <map>
#include <vector>
#include <functional>

class Character;

#pragma once

#define PRESS GLFW_PRESS
#define RELEASE GLFW_RELEASE

typedef std::function<void(float)> axisCallback;
typedef std::function<void()> actionCallback;

class InputSystem {

    public:
        InputSystem();
        ~InputSystem();

        std::map<std::string, std::vector<axisCallback> > axis;
        std::map<std::string, std::vector<std::function<void()>>> pressActions;
        std::map<std::string, std::vector<std::function<void()>>> releaseActions;

        void KeyCallback(int key, int action);
        void CursorCallback(double xPos, double yPos);
        void MouseButton(int button, int action);
        void UnbindAxis (std::string name, int index);
        void UnbindPressAction (std::string name, int index);
        void UnbindReleaseAction (std::string name, int index);
        void executeAxis(std::string name, int value);
        void executePressActions(std::string name, int value);
        void executeReleaseActions(std::string name, int value);

        template<class T>
        int BindAxis (std::string name, T* const object, void(T::* const axisCallback)(float movement)) {
            using namespace std::placeholders;
            axis[name].emplace_back(std::bind(axisCallback, object, _1));
            return axis[name].size() - 1;
        }

        template<class T>
        int BindAction (std::string name, int action, T* const object, void(T::* const actionCallback)()) {
            if(action == PRESS) {
                pressActions[name].emplace_back(std::bind(actionCallback, object));
                return pressActions[name].size() - 1;
            }
            else if(action == RELEASE) {
                releaseActions[name].emplace_back(std::bind(actionCallback, object));
                return releaseActions[name].size() - 1;
            }
        }

    private:
};