#pragma once

#include "../../components/headers/CollisionComponent.hpp"
#include "../../components/headers/BoxCollisionComponent.hpp"
#include <vector>
#include <math.h>

using namespace std;

class CollisionSystem {
private:
    std::vector<CollisionComponent *> Colisiones;
    std::vector<BoxCollisionComponent *> BoxColisiones;
public:

    double AreaOverlapBoxCollision(BoxCollisionComponent * CollisionA, BoxCollisionComponent * CollisionB);
    int TypeOfOverlapDimension(float R1ND, float R1PD, float R2ND, float R2PD);
    int TypeOfOverlap(Vector R1N, Vector R1P, Vector R2N, Vector R2P);//recibo vectors
    int TypeOfOverlap(BoxCollisionComponent * CollisionA, BoxCollisionComponent * CollisionB);//recibo vectors
    void AddCollision(CollisionComponent * Collision);
    void AddBoxCollision(BoxCollisionComponent * Collision);
    void RemoveBoxCollision(BoxCollisionComponent * Collision);
    void CheckOverlaps();
    CollisionSystem();
    ~CollisionSystem();
};

//las colisiones se deben comprobar despues de hacer un render, esto debido a que es necesairo obtener las posiciones globales de todos los componentes
//es decir, necesito ejecutar la funcion render de los transfortion, y guardar las matrices, con las posicino y rotaciones globales de ser posible.
//no me gusta este metodo