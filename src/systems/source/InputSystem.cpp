#include "../headers/InputSystem.hpp"


InputSystem::InputSystem () {
    std::vector<std::function<void(float)>> axisInitializer = {};
    std::vector<std::function<void()>> actionInitializer = {};
    axis.insert(std::make_pair("moveForward", axisInitializer));
    axis.insert(std::make_pair("moveRight", axisInitializer));
    axis.insert(std::make_pair("x", axisInitializer));
    axis.insert(std::make_pair("y", axisInitializer));
    pressActions.insert(std::make_pair("pause", actionInitializer));
    releaseActions.insert(std::make_pair("pause", actionInitializer));
    pressActions.insert(std::make_pair("shoot", actionInitializer));
    releaseActions.insert(std::make_pair("shoot", actionInitializer));
    pressActions.insert(std::make_pair("defense", actionInitializer));
    releaseActions.insert(std::make_pair("defense", actionInitializer));
    pressActions.insert(std::make_pair("run", actionInitializer));
    releaseActions.insert(std::make_pair("run", actionInitializer));

}

InputSystem::~InputSystem () {

}

void InputSystem::UnbindAxis (std::string name, int index) {
    auto function = axis[name][index];
    if (function) {
        axis[name][index] = nullptr;
    }
}

void InputSystem::UnbindPressAction (std::string name, int index) {
    auto function = pressActions[name][index];
    if (function) {
        pressActions[name][index] = nullptr;
    }
}

void InputSystem::UnbindReleaseAction (std::string name, int index) {
    auto function = releaseActions[name][index];
    if (function) {
        releaseActions[name][index] = nullptr;
    }
}

void InputSystem::executeAxis (std::string name, int value) {
    for (const auto& bindfunction : axis[name]){
        if (bindfunction) {
            bindfunction(value);
        }
    }
}

void InputSystem::executeReleaseActions (std::string name, int value) {
    for (const auto& bindfunction : releaseActions[name]){
        if (bindfunction) {
            bindfunction();
        }
    }
}

void InputSystem::executePressActions (std::string name, int value) {
    for (const auto& bindfunction : pressActions[name]){
        if (bindfunction) {
            bindfunction();
        }
    }
}

void InputSystem::CursorCallback (double xPos, double yPos) {
    // axis["x"] = xPos;
    // axis["y"] = yPos;
}

void InputSystem::MouseButton (int button, int action) {
    // Right Click
    if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_PRESS ) {
        executePressActions("shoot", 1);
    }
    if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_RELEASE) {
        executeReleaseActions("shoot", 0);
    }
    // Left Click
    if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS) {
        
    }
    if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_RELEASE) {
        
    }
}

// TODO: Try to make a map
void InputSystem::KeyCallback (int key, int action) {
    // W
    if ( key == GLFW_KEY_W && action == GLFW_PRESS) {
        executeAxis("moveForward", 1);
    }
    else if ( key == GLFW_KEY_W && action == GLFW_RELEASE) {
        executeAxis("moveForward", 0);
    }
    // A
    else if ( key == GLFW_KEY_S && action == GLFW_PRESS) {
        executeAxis("moveForward", -1);
    }
    else if ( key == GLFW_KEY_S && action == GLFW_RELEASE) {
        executeAxis("moveForward", 0);
    }
    // S
    else if ( key == GLFW_KEY_A && action == GLFW_PRESS) {
        executeAxis("moveRight", -1);
    }
    else if ( key == GLFW_KEY_A && action == GLFW_RELEASE) {
        executeAxis("moveRight", 0);
    }
    // D
    else if ( key == GLFW_KEY_D && action == GLFW_PRESS) {
        executeAxis("moveRight", 1);
    }
    else if ( key == GLFW_KEY_D && action == GLFW_RELEASE) {
        executeAxis("moveRight", 0);
    }
    // Up
    // else if ( key == GLFW_KEY_UP && action == GLFW_PRESS) {
    //     axis["primaryDirection"] = 1;
    // }
    // else if ( key == GLFW_KEY_UP && action == GLFW_RELEASE) {
    //     axis["primaryDirection"] = 0;
    // }
    // // Down
    // else if ( key == GLFW_KEY_DOWN && action == GLFW_PRESS) {
    //     axis["primaryDirection"] = -1;
    // }
    // else if ( key == GLFW_KEY_DOWN && action == GLFW_RELEASE) {
    //     axis["primaryDirection"] = 0;
    // }
    // // Left
    // else if ( key == GLFW_KEY_LEFT && action == GLFW_PRESS) {
    //     axis["secondaryDirection"] = 1;
    // }
    // else if ( key == GLFW_KEY_LEFT && action == GLFW_RELEASE) {
    //     axis["secondaryDirection"] = 0;
    // }
    // // Right
    // else if ( key == GLFW_KEY_RIGHT && action == GLFW_PRESS) {
    //     axis["secondaryDirection"] = -1;
    // }
    // else if ( key == GLFW_KEY_RIGHT && action == GLFW_RELEASE) {
    //     axis["secondaryDirection"] = 0;
    // }
    // // ESC
    // else if ( key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
    //     actions["pause"] = 1;
    // }
    // else if ( key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE) {
    //     actions["pause"] = 0;
    // }
    // // SHOOT
    // else if ( key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
    //     actions["shoot"] = 1;
    // }
    // else if ( key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE) {
    //     actions["shoot"] = 0;
    // }
    // RUN
    else if ( key == GLFW_KEY_LEFT_SHIFT && action == GLFW_PRESS) {
        executePressActions("run", 1);
    }
    else if ( key == GLFW_KEY_LEFT_SHIFT && action == GLFW_RELEASE) {
        executeReleaseActions("run", 0);
    }
}