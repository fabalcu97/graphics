#include "../headers/CollisionSystem.hpp"
#include "../../actors/headers/Actor.hpp"
#include <iostream>

CollisionSystem::CollisionSystem() {
}

CollisionSystem::~CollisionSystem() {
}

void CollisionSystem::AddCollision(CollisionComponent * Colision){
    //deberia agregarlo de forma unica
    Colisiones.push_back(Colision);
}

void CollisionSystem::AddBoxCollision(BoxCollisionComponent * Collision){
    //std::cout << "agregando box collision" << std::endl;
    BoxColisiones.push_back(Collision);
}

void CollisionSystem::RemoveBoxCollision(BoxCollisionComponent * Collision){
    std::cout << "removiendo box collision" << std::endl;
    vector<BoxCollisionComponent *>::iterator it;
    it = find(BoxColisiones.begin(), BoxColisiones.end(), Collision);
    BoxColisiones.erase(it);
    //no basta con borrarla, necesito hacer des registrarla delos componentes con los que colision
    vector<CollisionComponent * > OtherComponents = Collision->GetOverlappedComponents();
    for(int i = 0; i < OtherComponents.size(); i++){
        OtherComponents[i]->EndOverlapComponent(Collision, dynamic_cast<Actor *>(Collision->Owner));//por ahora llamare con null
    }//elimando del otro lado
}


void CollisionSystem::CheckOverlaps(){
    //std::cout << "Comprobando collisiones" << std::endl;
    for(int i = 0; i < BoxColisiones.size(); i++){
        //como mnejar los distitntos tipso de colision, spherea subos, etc para que se comprueben con la misma funcion?
        for(int j = i+1; j < BoxColisiones.size(); j++){
            int Overlap = TypeOfOverlap(BoxColisiones[i], BoxColisiones[j]);
            //std::cout << i << "-" << j << ": " << Overlap << std::endl;
            if(Overlap && !BoxColisiones[i]->IsOverlappedWith(BoxColisiones[j])){
                //llamo la begin
                BoxColisiones[i]->BeginOverlapComponent(BoxColisiones[j], dynamic_cast<Actor *>(BoxColisiones[j]->Owner));//por ahora llamare con null
                BoxColisiones[j]->BeginOverlapComponent(BoxColisiones[i], dynamic_cast<Actor *>(BoxColisiones[i]->Owner));//por ahora llamare con null
            }
            else if(!Overlap && BoxColisiones[i]->IsOverlappedWith(BoxColisiones[j])){
                BoxColisiones[i]->EndOverlapComponent(BoxColisiones[j], dynamic_cast<Actor *>(BoxColisiones[j]->Owner));//por ahora llamare con null
                BoxColisiones[j]->EndOverlapComponent(BoxColisiones[i], dynamic_cast<Actor *>(BoxColisiones[i]->Owner));//por ahora llamare con null
                //llamo en end
            }
            //else no hago nada, se mantienen
        }
    }
}


double CollisionSystem::AreaOverlapBoxCollision(BoxCollisionComponent * CollisionA, BoxCollisionComponent * CollisionB){
//double XTree::OverlapRegions(vector<double>& N1, vector<double>& P1, vector<double>& N2, vector<double>& P2) {
    double acum = 1;

    Vector PA = CollisionA->GetPositvePoint();
    Vector PB = CollisionB->GetPositvePoint();
    Vector NA = CollisionA->GetNegativePoint();
    Vector NB = CollisionB->GetNegativePoint();

    double sobreposicion = std::min(PA.X, PB.X) - std::max(NA.X, NB.X);
    if (sobreposicion <= 0) {
        acum *= 0;
    }
    else {
        acum *= sobreposicion;
    }

    sobreposicion = std::min(PA.Y, PB.Y) - std::max(NA.Y, NB.Y);
    if (sobreposicion <= 0) {
        acum *= 0;
    }
    else {
        acum *= sobreposicion;
    }

    sobreposicion = std::min(PA.Z, PB.Z) - std::max(NA.Z, NB.Z);
    if (sobreposicion <= 0) {
        acum *= 0;
    }
    else {
        acum *= sobreposicion;
    }

    return acum;
}

int CollisionSystem::TypeOfOverlapDimension(float R1ND, float R1PD, float R2ND, float R2PD){
    //0 si no hay interseccion
    //1 si hay alguna interseccion
    //2 si R1 esta dentro de R2, R1 es mas grade
    //3 si R2 esta dentro de R1, o lo que mismo, R1 es mas peque�a que R2
    // en el que caso de que las regiones sean giuales, R1 sera siempre "mas grande"que R2, esto ya que estoy usando R! como criterio de busqueda entonces quiero que cuando sean iguales todo R2 sea tomado en cuenta y ya no tenga que revisar en sus hijos si hay overlap o no
    // este ultimo criterio tambien debe ser plicado dimension por dimension
    int TypeOverlap;
    if (R1ND <= R2ND) {
        if (R2ND <= R1PD) {
            if (R1PD < R2PD) {
                TypeOverlap = 1;
            }
            else {
                TypeOverlap = 3;
            }
        }
        else {//R2ND >= R1PD
            TypeOverlap = 0;
        }
    }
    else {// R2ND < R1ND
        if (R1ND <= R2PD) {
            if (R2PD < R1PD) {
                TypeOverlap = 1;
            }
            else {
                TypeOverlap = 2;
            }
        }
        else {
            TypeOverlap = 0;
        }
    }
    return TypeOverlap;
}

int CollisionSystem::TypeOfOverlap(Vector R1N, Vector R1P, Vector R2N, Vector R2P){//recibo vectors
    //0 si no hay interseccion
    //1 si hay alguna interseccion
    //2 si R1 esta dentro de R2, R1 es mas grade
    //3 si R2 esta dentro de R1, o lo que mismo, R1 es mas peque�a que R2
    int TypeOverlapDimension = TypeOfOverlapDimension(R1N.X, R1P.X, R2N.X, R2P.X);
    int TypeOverlap = TypeOverlapDimension;

    TypeOverlapDimension = TypeOfOverlapDimension(R1N.Y, R1P.Y, R2N.Y, R2P.Y);
    if (TypeOverlap == 3 && TypeOverlapDimension == 2) {
        TypeOverlap = 1;
    }
    else if (TypeOverlap == 2 && TypeOverlapDimension == 3) {
        TypeOverlap = 1;
    }
    else {
        TypeOverlap = std::min(TypeOverlap, TypeOverlapDimension);
    }

    TypeOverlapDimension = TypeOfOverlapDimension(R1N.Z, R1P.Z, R2N.Z, R2P.Z);
    if (TypeOverlap == 3 && TypeOverlapDimension == 2) {
        TypeOverlap = 1;
    }
    else if (TypeOverlap == 2 && TypeOverlapDimension == 3) {
        TypeOverlap = 1;
    }
    else {
        TypeOverlap = std::min(TypeOverlap, TypeOverlapDimension);
    }

    return TypeOverlap;
}

int CollisionSystem::TypeOfOverlap(BoxCollisionComponent * CollisionA, BoxCollisionComponent * CollisionB){//recibo vectors
    return TypeOfOverlap(CollisionA->GetNegativePoint(), CollisionA->GetPositvePoint(), CollisionB->GetNegativePoint(), CollisionB->GetPositvePoint());
}