# Declaration of variables
CC = g++
LINUX_FLAGS =-lglfw -lGL -lGLU -lX11 -lpthread -lXrandr -lXi -w
MACOS_FLAGS=-lglfw -framework OpenGL -w
CC_FLAGS=

UNAME_S := $(shell uname -s)
ifeq ($(UNAME_S),Linux)
	CC_FLAGS=$(LINUX_FLAGS)
endif
ifeq ($(UNAME_S),Darwin)
	CC_FLAGS=$(MACOS_FLAGS)
endif


# File names
EXEC = run
SOURCES = $(wildcard src/structures/source/*.cpp) \
			$(wildcard src/components/source/*.cpp) \
			$(wildcard src/systems/source/*.cpp) \
			$(wildcard src/engine/source/*.cpp) \
			$(wildcard src/engine/*.cpp) \
			$(wildcard src/actors/source/*.cpp) \
			$(wildcard src/*.cpp)
			
OBJECTS = $(SOURCES:.cpp=.o)

 
# Main target
$(EXEC): $(OBJECTS)
	$(CC) -std=c++17 $(OBJECTS) -o $(EXEC).out $(CC_FLAGS)
	make clean
 
# To obtain object files
%.o: %.cpp
	$(CC) -c -std=c++17 $< -o $@ $(CC_FLAGS)
 
# To remove generated files
OBJECTSE = $(wildcard src/structures/source/*.o) \
			$(wildcard src/components/source/*.o) \
			$(wildcard src/actors/source/*.o) \
			$(wildcard src/systems/source/*.o) \
			$(wildcard src/engine/source/*.o) \
			$(wildcard src/engine/*.o) \
			$(wildcard src/*.o)

clean:
	rm -f $(EXEC) $(OBJECTSE)