#include <iostream>


class A {
    public:
        int a;
        A();
        ~A(){};
        void modifyA(int);
        void setValue();
        void showA();
};

typedef void(A::*otherClassMethod)(int);

class B {
    public:
        B(){};
        ~B(){};
        otherClassMethod methodToUse;
        A* objectToUse;
        void someFunction ();
        void assignMethod (otherClassMethod method, A* object);
};

void B::assignMethod (otherClassMethod method, A* object) {
    objectToUse = object;
    methodToUse = method;
    
}
void B::someFunction () {
    (*objectToUse.*methodToUse)(rand());
}

B b;

A::A(){
    b.assignMethod(&A::modifyA, this);
};
void A::modifyA(int value) {
    a = value;
}

void A::showA() {
    std::cout << "A =>" << a << std::endl;
}



int main () {
    srand(time(NULL));

    A a;

    a.showA();
    b.someFunction();
    a.showA();
};